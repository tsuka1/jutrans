

!#define DEBUG


!#define MODULE_TEST
#ifdef MODULE_TEST
#define DEBUG
#endif


!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!-- mod_cgsolver
!--
!> @brief Fortran module for conjugate gradient method for linear equations.
!> @author Shigeru Tsukamoto (s.tsukamoto@fz-juelich.de)
!--
module mod_cgsolver


  !--
  !-- Module
  !--
  use mod_parameter, only : NX, NY, NZ
  implicit none
  private


  !--
  !-- Module parameter
  !--
  integer, private :: i
  real(8), private, parameter :: EPSILON                 = 1.0d-11 !< Tolerance for relative residual: rr = ||r||2/||x||2
  integer, private, parameter :: RESTART_MAX             = 3
  integer, private, parameter :: ITER_MAX(0:RESTART_MAX) = [ int( sqrt( dble( NX * NY * NZ ) ) ), ( NX * NY * NZ, i = 1, RESTART_MAX ) ]


  !--
  !-- Scope of subprogram
  !--
  public :: cgsolver_exec


  !--
  !-- Interface
  !--
  interface cgsolver_exec
    module procedure cgsolver_exec_d
    module procedure cgsolver_exec_z
  end interface cgsolver_exec


  contains


  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- cgsolver_exec_d
  !--
  subroutine cgsolver_exec_d( cart, fh, b, energy, ispin, vloc, npp, pp, x, itime, ierr, err, errstr )


    !--
    !-- Module
    !--
    use mod_mpi
    use mod_parameter,       only : NCPX, NCPY, NCPZ
    use mod_nonlocal_data,   only : PPDECMP
    use mod_cgsolver_kake,   only : exec_cgsolver_kake, cgsolver_kake_init
    use mod_cgsolver_precon, only : exec_cgsolver_precon, cgsolver_precon_init
    implicit none


    !--
    !-- Parameter
    !--
    character(len=*), parameter :: PROMPT = "CGSOLVER_EXEC_D >>>"


    !--
    !-- Dummy arguments
    !--
    integer,          intent(in)    :: cart !< MPI Cartesian communicator of processes involved in this routine
    integer,          intent(in)    :: fh   !< MPI-IO file handler for standard output
    real(8),          intent(in)    :: b(NCPX,NCPY,NCPZ)
    real(8),          intent(in)    :: energy
    integer,          intent(in)    :: ispin
    real(8),          intent(in)    :: vloc(NCPX,NCPY,NCPZ)
    integer,          intent(in)    :: npp
    type(PPDECMP),    intent(in)    :: pp(npp)
    real(8),          intent(inout) :: x(NCPX,NCPY,NCPZ)
    integer,          intent(out)   :: itime
    integer,          intent(out)   :: ierr
    real(8),          intent(out)   :: err
    character(len=*), intent(out)   :: errstr


    !--
    !-- Local variable
    !--
    integer :: cart_rank
    logical :: o
    real(8) :: time_start, time_end
    logical :: lastloop

    real(8) :: fnorm_b, fnorm_x, fnorm_r, rr, trr
    real(8) :: srnew, pap, alpha, srold, beta
    real(8) :: r(NCPX,NCPY,NCPZ), s(NCPX,NCPY,NCPZ), p(NCPX,NCPY,NCPZ), ax(NCPX,NCPY,NCPZ), ap(NCPX,NCPY,NCPZ)


    !--
    !-- Work variables
    !--
    integer :: ix, iy, iz, iter, irestart, itertotal


    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


    !--
    !-- Initialize
    !--
    ierr      = 0
    err       = 0.0d0
    errstr    = ""
    lastloop  = .false.
    itertotal = 0
    call MPI_Comm_rank( cart, cart_rank, ierr )
    o = ( fh /= MPI_FILE_NULL .and. cart_rank == 0 )
    call cgsolver_precon_init( cart, fh, ierr )
    call cgsolver_kake_init( cart, fh, ierr )
    call MPI_Barrier( cart, ierr )
    time_start = MPI_Wtime()


    !$omp parallel default(shared) private(irestart,iter)


    !-- Frobenius norm of b: fnorm_b = ||b||2
    !$omp single
    fnorm_b = 0.0d0
    !$omp end single
    !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_b)
    do iz = 1, NCPZ
    do iy = 1, NCPY
    do ix = 1, NCPX
      fnorm_b = fnorm_b + b(ix,iy,iz) ** 2
    end do !-- ix
    end do !-- iy
    end do !-- iz
    !$omp end do
    !$omp single
    call MPI_Allreduce( MPI_IN_PLACE, fnorm_b, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
    fnorm_b = sqrt( fnorm_b )
    !$omp end single


    !-- Loop for restart
    restart: do irestart = 0, RESTART_MAX


      !-- ax = H * x
      call exec_cgsolver_kake( cart, x(:,:,:), energy, ispin, vloc(:,:,:), npp, pp(:), ax(:,:,:) )


      !-- Initial r vector: r = b - ax
      !$omp do collapse(3) private(iz,iy,ix)
      do iz = 1, NCPZ
      do iy = 1, NCPY
      do ix = 1, NCPX
        r(ix,iy,iz) = b(ix,iy,iz) - ax(ix,iy,iz)
      end do !-- ix
      end do !-- iy
      end do !-- iz
      !$omp end do


      !-- Precondition: s = K * r
      call exec_cgsolver_precon( cart, r(:,:,:), s(:,:,:) )


      !-- srnew = <s|r>
      !$omp single
      srnew = 0.0d0
      !$omp end single
      !$omp do collapse(3) private(iz,iy,ix) reduction(+:srnew)
      do iz = 1, NCPZ
      do iy = 1, NCPY
      do ix = 1, NCPX
        srnew = srnew + s(ix,iy,iz) * r(ix,iy,iz)
      end do !-- ix
      end do !-- iy
      end do !-- iz
      !$omp end do
      !$omp single
      call MPI_Allreduce( MPI_IN_PLACE, srnew, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
      !$omp end single


      !-- Initial p vector: p = s
      !$omp do collapse(3) private(iz,iy,ix)
      do iz = 1, NCPZ
      do iy = 1, NCPY
      do ix = 1, NCPX
        p(ix,iy,iz) = s(ix,iy,iz)
      end do !-- ix
      end do !-- iy
      end do !-- iz
      !$omp end do


      !-- Loop for CG iteration
      iteration: do iter = 1, ITER_MAX(irestart)


        !-- ap = H * p
        call exec_cgsolver_kake( cart, p(:,:,:), energy, ispin, vloc(:,:,:), npp, pp(:), ap(:,:,:) )


        !-- pap = <p|ap>
        !-- alpha = srnew / pap
        !$omp single
        pap = 0.0d0
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:pap)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          pap = pap + p(ix,iy,iz) * ap(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, pap, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
        alpha = srnew / pap
        !$omp end single


        !-- new x vector: x = x + alpha * p
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          x(ix,iy,iz) = x(ix,iy,iz) + alpha * p(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do nowait


        !-- Frobenius norm of x: fnorm_x = ||x||2
        !$omp single
        fnorm_x = 0.0d0
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_x)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          fnorm_x = fnorm_x + x(ix,iy,iz) ** 2
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, fnorm_x, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
        fnorm_x = sqrt( fnorm_x )
        !$omp end single


        !-- new ax vector: ax = ax + alpha * ap
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          ax(ix,iy,iz) = ax(ix,iy,iz) + alpha * ap(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do nowait


        !-- new r vector: r = b -ax
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          r(ix,iy,iz) = b(ix,iy,iz) - ax(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do


        !-- Precondition
        call exec_cgsolver_precon( cart, r(:,:,:), s(:,:,:) )


        !-- srold = srnew
        !-- srnew = <s|r>
        !$omp single
        srold = srnew
        srnew = 0.0d0
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:srnew)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          srnew = srnew + s(ix,iy,iz) * r(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, srnew, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
        !$omp end single


        !-- Relative residual: rr = ||r||2/||x||2
        !$omp single
        fnorm_r = 0.0d0
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_r)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          fnorm_r = fnorm_r + r(ix,iy,iz) ** 2
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, fnorm_r, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
        fnorm_r = sqrt( fnorm_r )
        rr = fnorm_r / fnorm_x
        !$omp end single


        !-- Check convergence
#ifdef MODULE_TEST
        !$omp single
        call MPI_Barrier( cart, ierr )
        if( o ) call MPIX_File_write_shared( fh, '(a,x,"iter =",x,i4,x,"log(||r||2) =",x,f6.2,x,"log(||r||2/||x||2) =",x,f6.2)', &
       &                                     PROMPT, iter, [ log10( fnorm_r ), log10( rr ) ], ierr )
        !$omp end single
#endif
        if( rr < EPSILON ) exit iteration


        !-- Beta = <s|r>_new / <s|r>_old
        !$omp single
        beta = srnew / srold
        !$omp end single


        !-- new p vector: p = s + beta * p
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          p(ix,iy,iz) = s(ix,iy,iz) + beta * p(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do


      end do iteration !-- iter


      !$omp single
      itertotal = itertotal + min( iter, ITER_MAX(irestart) )
      !$omp end single


      if( rr < EPSILON )then
        if( lastloop ) exit restart
        !$omp barrier
        lastloop = .true.
      end if !-- rr


    end do restart !-- irestart


    !$omp end parallel


    call MPI_Barrier( cart, ierr )
    if( rr > EPSILON )then
      ierr = -1 * itertotal
      err  = rr
      write( unit=errstr, fmt='(a,x,a,x,i8,x,a,x,f6.2)' ) PROMPT, "ERROR: not converged. CG i =", itertotal, "log[RR] =", log10( rr )
      return
    end if


    !$omp parallel default(shared)


    !-- Frobenius norm of x: fnorm_x = ||x||2
    !$omp single
    fnorm_x = 0.0d0
    !$omp end single
    !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_x)
    do iz = 1, NCPZ
    do iy = 1, NCPY
    do ix = 1, NCPX
      fnorm_x = fnorm_x + x(ix,iy,iz) ** 2
    end do !-- ix
    end do !-- iy
    end do !-- iz
    !$omp end do
    !$omp single
    call MPI_Allreduce( MPI_IN_PLACE, fnorm_x, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
    fnorm_x = sqrt( fnorm_x )
    !$omp end single


    !-- True Relative Residual (TRR): trr = ||b-Ax||2/||x||2
    call exec_cgsolver_kake( cart, x(:,:,:), energy, ispin, vloc(:,:,:), npp, pp(:), ax(:,:,:) )
    !$omp single
    fnorm_r = 0.0d0
    !$omp end single
    !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_r)
    do iz = 1, NCPZ
    do iy = 1, NCPY
    do ix = 1, NCPX
      fnorm_r = fnorm_r + ( b(ix,iy,iz) - ax(ix,iy,iz) ) ** 2
    end do !-- ix
    end do !-- iy
    end do !-- iz
    !$omp end do
    !$omp single
    call MPI_Allreduce( MPI_IN_PLACE, fnorm_r, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
    fnorm_r = sqrt( fnorm_r )
    trr = fnorm_r / fnorm_x
    !$omp end single


    !$omp end parallel


    !-- Finalize
    call MPI_Barrier( cart, ierr )
    time_end = MPI_Wtime()
    itime = int( time_end - time_start )
    ierr  = itertotal
    err   = trr
    write( unit=errstr, fmt='(a,x,i6,x,a,x,i6,2(x,a,x,f6.2)$)' ) "CG i =", itertotal, "Wtime (sec) =", int( time_end - time_start ), &
   &  "log[RR] =", log10( rr ), "log[TRR] =", log10( trr )
#ifdef MODULE_TEST
    print '(a,x,"cart_rank =",x,i4,x,a)', PROMPT, cart_rank, errstr
    call MPI_Barrier( cart, ierr )
    stop PROMPT // " Module Test: OK."
#endif
    return


  end subroutine cgsolver_exec_d
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0



  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- cgsolver_exec_z
  !--
  subroutine cgsolver_exec_z( cart, fh, b, energy, ispin, vloc, npp, pp, skx, sky, skz, x, itime, ierr, err, errstr )


    !--
    !-- Module
    !--
    use mod_mpi
    use mod_parameter,       only : NCPX, NCPY, NCPZ
    use mod_nonlocal_data,   only : PPDECMP
    use mod_cgsolver_kake,   only : exec_cgsolver_kake, cgsolver_kake_init
    use mod_cgsolver_precon, only : exec_cgsolver_precon, cgsolver_precon_init
    implicit none


    !--
    !-- Parameter
    !--
    character(len=*), parameter :: PROMPT = "CGSOLVER_EXEC_Z >>>"


    !--
    !-- Dummy arguments
    !--
    integer,          intent(in)    :: cart !< MPI Cartesian communicator of processes involved in this routine
    integer,          intent(in)    :: fh   !< MPI-IO file handler for standard output
    complex(8),       intent(in)    :: b(NCPX,NCPY,NCPZ)
    real(8),          intent(in)    :: energy
    integer,          intent(in)    :: ispin
    real(8),          intent(in)    :: vloc(NCPX,NCPY,NCPZ)
    integer,          intent(in)    :: npp
    type(PPDECMP),    intent(in)    :: pp(npp)
    real(8),          intent(in)    :: skx
    real(8),          intent(in)    :: sky
    complex(8),       intent(in)    :: skz
    complex(8),       intent(inout) :: x(NCPX,NCPY,NCPZ)
    integer,          intent(out)   :: itime
    integer,          intent(out)   :: ierr
    real(8),          intent(out)   :: err
    character(len=*), intent(out)   :: errstr


    !--
    !-- Local variable
    !--
    integer :: cart_rank
    logical :: o
    real(8) :: time_start, time_end
    logical :: lastloop

    real(8)    :: fnorm_b, fnorm_x, fnorm_r, rr, trr
    complex(8) :: srnew, pap, alpha, srold, beta
    complex(8) :: r(NCPX,NCPY,NCPZ), s(NCPX,NCPY,NCPZ), p(NCPX,NCPY,NCPZ), ax(NCPX,NCPY,NCPZ), ap(NCPX,NCPY,NCPZ)


    !--
    !-- Work variables
    !--
    integer    :: ix, iy, iz, iter, irestart, itertotal


    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


    !--
    !-- Initialize
    !--
    ierr      = 0
    err       = 0.0d0
    errstr    = ""
    lastloop  = .false.
    itertotal = 0
    call MPI_Comm_rank( cart, cart_rank, ierr )
    o = ( fh /= MPI_FILE_NULL .and. cart_rank == 0 )
    call cgsolver_precon_init( cart, fh, ierr )
    call cgsolver_kake_init( cart, fh, ierr )
    call MPI_Barrier( cart, ierr )
    time_start = MPI_Wtime()


    !$omp parallel default(shared) private(irestart,iter)


    !-- Frobenius norm of b: fnorm_b = ||b||2
    !$omp single
    fnorm_b = 0.0d0
    !$omp end single
    !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_b)
    do iz = 1, NCPZ
    do iy = 1, NCPY
    do ix = 1, NCPX
      fnorm_b = fnorm_b + abs( b(ix,iy,iz) ) ** 2
    end do !-- ix
    end do !-- iy
    end do !-- iz
    !$omp end do
    !$omp single
    call MPI_Allreduce( MPI_IN_PLACE, fnorm_b, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
    fnorm_b = sqrt( fnorm_b )
    !$omp end single


    !-- Loop for restart
    restart: do irestart = 0, RESTART_MAX


      !-- ax = H * x
      call exec_cgsolver_kake( cart, x(:,:,:), energy, ispin, vloc(:,:,:), npp, pp(:), skx, sky, skz, ax(:,:,:) )


      !-- Initial r vector: r = b - ax
      !$omp do collapse(3) private(iz,iy,ix)
      do iz = 1, NCPZ
      do iy = 1, NCPY
      do ix = 1, NCPX
        r(ix,iy,iz) = b(ix,iy,iz) - ax(ix,iy,iz)
      end do !-- ix
      end do !-- iy
      end do !-- iz
      !$omp end do


      !-- Precondition: s = K * r
      call exec_cgsolver_precon( cart, r(:,:,:), s(:,:,:) )


      !-- srnew = <s|r>
      !$omp single
      srnew = dcmplx( 0.0d0, 0.0d0 )
      !$omp end single
      !$omp do collapse(3) private(iz,iy,ix) reduction(+:srnew)
      do iz = 1, NCPZ
      do iy = 1, NCPY
      do ix = 1, NCPX
        srnew = srnew + conjg( s(ix,iy,iz) ) * r(ix,iy,iz)
      end do !-- ix
      end do !-- iy
      end do !-- iz
      !$omp end do
      !$omp single
      call MPI_Allreduce( MPI_IN_PLACE, srnew, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, cart, ierr )
      !$omp end single


      !-- Initial p vector: p = s
      !$omp do collapse(3) private(iz,iy,ix)
      do iz = 1, NCPZ
      do iy = 1, NCPY
      do ix = 1, NCPX
        p(ix,iy,iz) = s(ix,iy,iz)
      end do !-- ix
      end do !-- iy
      end do !-- iz
      !$omp end do


      !-- Loop for CG iteration
      iteration: do iter = 1, ITER_MAX(irestart)


        !-- ap = H * p
        call exec_cgsolver_kake( cart, p(:,:,:), energy, ispin, vloc(:,:,:), npp, pp(:), skx, sky, skz, ap(:,:,:) )


        !-- pap = <p|ap>
        !-- alpha = srnew / pap
        !$omp single
        pap = dcmplx( 0.0d0, 0.0d0 )
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:pap)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          pap = pap + conjg( p(ix,iy,iz) ) * ap(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, pap, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, cart, ierr )
        alpha = srnew / pap
        !$omp end single


        !-- new x vector: x = x + alpha * p
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          x(ix,iy,iz) = x(ix,iy,iz) + alpha * p(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do nowait


        !-- Frobenius norm of x: fnorm_x = ||x||2
        !$omp single
        fnorm_x = 0.0d0
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_x)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          fnorm_x = fnorm_x + abs( x(ix,iy,iz) ) ** 2
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, fnorm_x, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
        fnorm_x = sqrt( fnorm_x )
        !$omp end single


        !-- new ax vector: ax = ax + alpha * ap
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          ax(ix,iy,iz) = ax(ix,iy,iz) + alpha * ap(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do nowait


        !-- new r vector: r = b -ax
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          r(ix,iy,iz) = b(ix,iy,iz) - ax(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do


        !-- Precondition: s = K * r
        call exec_cgsolver_precon( cart, r(:,:,:), s(:,:,:) )


        !-- srold = srnew
        !-- srnew = <s|r>
        !$omp single
        srold = srnew
        srnew = dcmplx( 0.0d0, 0.0d0 )
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:srnew)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          srnew = srnew + conjg( s(ix,iy,iz) ) * r(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, srnew, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, cart, ierr )
        !$omp end single


        !-- Relative residual: rr = ||r||2/||x||2
        !$omp single
        fnorm_r = 0.0d0
        !$omp end single
        !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_r)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          fnorm_r = fnorm_r + abs( r(ix,iy,iz) ) ** 2
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do
        !$omp single
        call MPI_Allreduce( MPI_IN_PLACE, fnorm_r, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
        fnorm_r = sqrt( fnorm_r )
        rr = fnorm_r / fnorm_x
        !$omp end single


        !-- Check convergence
#ifdef MODULE_TEST
        !$omp single
        call MPI_Barrier( cart, ierr )
        if( o ) call MPIX_File_write_shared( fh, '(a,x,"iter =",x,i4,x,"log(||r||2) =",x,f6.2,x,"log(||r||2/||x||2) =",x,f6.2)', &
       &                                     PROMPT, iter, [ log10( fnorm_r ), log10( rr ) ], ierr )
        !$omp end single
#endif
        if( rr < EPSILON ) exit iteration


        !-- Beta = <s|r>_new / <s|r>_old
        !$omp single
        beta = srnew / srold
        !$omp end single


        !-- new p vector: p = s + beta * p
        !$omp do collapse(3) private(iz,iy,ix)
        do iz = 1, NCPZ
        do iy = 1, NCPY
        do ix = 1, NCPX
          p(ix,iy,iz) = s(ix,iy,iz) + beta * p(ix,iy,iz)
        end do !-- ix
        end do !-- iy
        end do !-- iz
        !$omp end do


      end do iteration !-- iter


      !$omp single
      itertotal = itertotal + min( iter, ITER_MAX(irestart) )
      !$omp end single


      if( rr < EPSILON )then
        if( lastloop ) exit restart
        !$omp barrier
        lastloop = .true.
      end if !-- rr


    end do restart !-- irestart


    !$omp end parallel


    call MPI_Barrier( cart, ierr )
    if( rr > EPSILON )then
      ierr = -1 * itertotal
      err  = rr
      write( unit=errstr, fmt='(a,x,a,x,i8,x,a,x,f6.2)' ) PROMPT, "ERROR: not converged. CG i =", itertotal, "log[RR] =", log10( rr )
      return
    end if


    !$omp parallel default(shared)


    !-- Frobenius norm of x: fnorm_x = ||x||2
    !$omp single
    fnorm_x = 0.0d0
    !$omp end single
    !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_x)
    do iz = 1, NCPZ
    do iy = 1, NCPY
    do ix = 1, NCPX
      fnorm_x = fnorm_x + abs( x(ix,iy,iz) ) ** 2
    end do !-- ix
    end do !-- iy
    end do !-- iz
    !$omp end do
    !$omp single
    call MPI_Allreduce( MPI_IN_PLACE, fnorm_x, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
    fnorm_x = sqrt( fnorm_x )
    !$omp end single


    !-- True Relative Residual (TRR): trr = ||b-Ax||2/||x||2
    call exec_cgsolver_kake( cart, x(:,:,:), energy, ispin, vloc(:,:,:), npp, pp(:), skx, sky, skz, ax(:,:,:) )
    !$omp single
    fnorm_r = 0.0d0
    !$omp end single
    !$omp do collapse(3) private(iz,iy,ix) reduction(+:fnorm_r)
    do iz = 1, NCPZ
    do iy = 1, NCPY
    do ix = 1, NCPX
      fnorm_r = fnorm_r + abs( b(ix,iy,iz) - ax(ix,iy,iz) ) ** 2
    end do !-- ix
    end do !-- iy
    end do !-- iz
    !$omp end do
    !$omp single
    call MPI_Allreduce( MPI_IN_PLACE, fnorm_r, 1, MPI_DOUBLE_PRECISION, MPI_SUM, cart, ierr )
    fnorm_r = sqrt( fnorm_r )
    trr = fnorm_r / fnorm_x
    !$omp end single


    !$omp end parallel


    !-- Finalize
    call MPI_Barrier( cart, ierr )
    time_end = MPI_Wtime()
    itime = int( time_end - time_start )
    ierr  = itertotal
    err   = trr
    write( unit=errstr, fmt='(a,x,i6,x,a,x,i6,2(x,a,x,f6.2)$)' ) "CG i =", itertotal, "Wtime (sec) =", int( time_end - time_start ), &
   &  "log[RR] =", log10( rr ), "log[TRR] =", log10( trr )
#ifdef MODULE_TEST
    print '(a,x,"cart_rank =",x,i4,x,a)', PROMPT, cart_rank, errstr
    call MPI_Barrier( cart, ierr )
    stop PROMPT // " Module Test: OK."
#endif
    return


  end subroutine cgsolver_exec_z
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0



end module mod_cgsolver
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
