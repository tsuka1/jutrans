

!#define DEBUG


!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!-- mod_inversematrix.F90
!--
!> @brief Fortran module for inverse matrix
!> @author Shigeru Tsukamoto (s.tsukamoto@fz-juelich.de)
!> @date 30. Aug. 2016
!--
module mod_inversematrix


  !--
  !-- Module
  !--
  implicit none
  private


  !--
  !-- Subprogram
  !--
  public :: set_inversematrix
  interface set_inversematrix
    module procedure set_inversematrix_z
    module procedure set_inversematrix_z_old
  end interface set_inversematrix


  contains


  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- set_inversematrix_z
  !--
  !> @brief Fortran subroutine for inverting square matrix
  !> @author Shigeru Tsukamoto (s.tsukamoto@fz-juelich.de)
  !> @date 19. Dec. 2017
  !> @date 13. Nov. 2018 Call to MPIX_Abort is changed. (S.Tsukamoto)
  !> @details The subroutine inverts a square matrix. \f$X=X^{-1}\f$. Input array is overwritten by
  !>          the invert matrix.
  !--
  subroutine set_inversematrix_z( ctxt, fh, n, x, ix, jx, xdsc, ierr )


    !--
    !-- Module
    !--
    use mod_mpi
    use mod_scalapack, only : DLEN_, CTXT_, M_, N_, MB_, NB_, RSRC_, CSRC_
    use mod_blacs,     only : blacs_gridinfo, blacs_barrier
    use mod_scalapack, only : numroc, pzgetrf, pzgetri
    implicit none


    !--
    !-- Parameter
    !--
    character(len=*), parameter :: PROMPT = "SET_INVERSEMATRIX_Z >>>"


    !--
    !-- Dummy argment
    !--
    integer,    intent(in)    :: ctxt        !< BLACS context
    integer,    intent(in)    :: fh          !< MPI standard output file handler
    integer,    intent(in)    :: n           !< Dimension of subarray
    complex(8), intent(inout) :: x(:,:)      !< BLACS array for matrix to be inverted
    integer,    intent(in)    :: ix          !< Row index of the first element of subarray
    integer,    intent(in)    :: jx          !< Col index of the first element of subarray
    integer,    intent(in)    :: xdsc(DLEN_) !< BLACS descriptor for array x
    integer,    intent(out)   :: ierr        !< Error indicator


    !--
    !-- Local variable
    !--
    integer                         :: nprow, npcol, myprow, mypcol !< BLACS context information
    logical                         :: o                            !< I/O flag
    integer                         :: npiv                         !< Size of pivot array ipiv(:)
    integer,            allocatable :: ipiv(:)                      !< Pivot array
    character(len=128)              :: buf                          !< String buffer
    integer                         :: itmp(1)
    complex(8)                      :: ztmp(1)
    integer                         :: lzwork, liwork
    integer,            allocatable :: iwork(:)
    complex(8),         allocatable :: zwork(:)


    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


    !--
    !-- Check argument
    !--
    if( n < 1                  ) call MPIX_Abort( PROMPT // " ERROR: n < 1.",                  ierr )
    if( ix < 1                 ) call MPIX_Abort( PROMPT // " ERROR: ix < 1.",                 ierr )
    if( jx < 1                 ) call MPIX_Abort( PROMPT // " ERROR: jx < 1.",                 ierr )
    if( xdsc(CTXT_) /= ctxt    ) call MPIX_Abort( PROMPT // " ERROR: xdsc(CTXT_) /= ctxt.",    ierr )
    if( xdsc(M_) < ix + n - 1  ) call MPIX_Abort( PROMPT // " ERROR: xdsc(M_) < ix + n - 1.",  ierr )
    if( xdsc(N_) < jx + n - 1  ) call MPIX_Abort( PROMPT // " ERROR: xdsc(N_) < jx + n - 1.",  ierr )
    if( xdsc(MB_) /= xdsc(NB_) ) call MPIX_Abort( PROMPT // " ERROR: xdsc(MB_) /= xdsc(NB_).", ierr )


    !--
    !-- Initialize
    !--
    ierr = 0
    !-- Check BLACS context
    call blacs_gridinfo( ctxt, nprow, npcol, myprow, mypcol )
    if( any( [ nprow, npcol, myprow, mypcol ] == -1 ) ) call MPIX_Abort( PROMPT // " ERROR: invalid BLACS context.", ierr )
    !-- Set flag for standard output
    o = ( fh /= MPI_FILE_NULL .and. myprow == 0 .and. mypcol == 0 )
    !-- Allocate work array for pivot information of scalapack routine pzgetrf
    npiv = max( 1, numroc( xdsc(M_), xdsc(MB_), myprow, xdsc(RSRC_), nprow ) + xdsc(MB_) )
    allocate( ipiv(npiv), stat=ierr )
    if( ierr /= 0 ) call MPIX_Abort( PROMPT // " ERROR: memory allocation for ipiv failed.", ierr )


    !--
    !-- Execute LU factorization
    !--
#ifdef DEBUG
    if( o ) call MPIX_File_write_shared( fh, '(a,x,"Execute LU factorization.")', PROMPT, ierr )
    call blacs_barrier( ctxt, "All" )
#endif
    call pzgetrf( n, n, x, ix, jx, xdsc, ipiv, ierr )
    if( ierr /= 0 ) call MPIX_Abort( PROMPT // " ERROR: pagetrf returns error code. ierr = ", ierr, ierr )


    !--
    !-- Invert matrix
    !--
#ifdef DEBUG
    if( o ) call MPIX_File_write_shared( fh, '(a,x,"Invert matrix.")', PROMPT, ierr )
    call blacs_barrier( ctxt, "All" )
#endif
    !-- Determine optimied workspace size
    call pzgetri( n, x, ix, jx, xdsc, ipiv, ztmp,  -1,     itmp,  -1,     ierr )
    if( ierr /= 0 ) call MPIX_Abort( PROMPT // " ERROR: pzgetri returns error code. ierr = ", ierr, ierr )
    lzwork = max( 1, int( dble( ztmp(1) ) ) )
    liwork = max( 1, itmp(1) )
    !-- Allocate optimied workspace zwork & iwork
    allocate( zwork(lzwork), iwork(liwork), stat=ierr )
    if( ierr /= 0 ) call MPIX_Abort( PROMPT // " ERROR: memory allocation for zwork & iwork failed.", ierr )
    !-- Execute matrix inversion
    call pzgetri( n, x, ix, jx, xdsc, ipiv, zwork, lzwork, iwork, liwork, ierr )
    if( ierr /= 0 ) call MPIX_Abort( PROMPT // " ERROR: pzgetri returns error code. ierr = ", ierr, ierr )


    !--
    !-- Finalize
    !--
#ifdef DEBUG
    if( o ) call MPIX_File_write_shared( fh, '(a,x,"Finalize.")', PROMPT, ierr )
    call blacs_barrier( ctxt, "All" )
#endif
    deallocate( ipiv, zwork, iwork,  stat=ierr )
    return


  end subroutine set_inversematrix_z
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0



  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- set_inversematrix_z_old
  !--
  !> @brief Fortran subroutine for inverting square matrix
  !> @author Shigeru Tsukamoto (s.tsukamoto@fz-juelich.de)
  !> @date 30. Aug. 2016
  !> @details The subroutine inverts a square matrix. \f$X=X^{-1}\f$. Input array is overwritten by
  !>          the invert matrix.
  !--
  subroutine set_inversematrix_z_old( o, fh, ctxt, n, x, ix, jx, xdsc, ierr )


    !--
    !-- Module
    !--
    use mod_mpi
    use mod_scalapack, only : DLEN_, CTXT_, M_, N_, MB_, NB_, RSRC_, CSRC_
    use mod_blacs,     only : blacs_gridinfo, blacs_barrier
    use mod_scalapack, only : numroc, pzgetrf, pzgetri
    implicit none


    !--
    !-- Parameter
    !--
    character(len=*), parameter :: PROMPT = "SET_INVERSEMATRIX_Z_OLD >>>"


    !--
    !-- Dummy argment
    !--
    logical,    intent(in)    :: o           !< I/O standard output flag
    integer,    intent(in)    :: fh          !< MPI standard output file handler
    integer,    intent(in)    :: ctxt        !< BLACS context
    integer,    intent(in)    :: n           !< Dimension of subarray
    complex(8), intent(inout) :: x(:,:)      !< BLACS array for to be inverted
    integer,    intent(in)    :: ix          !< Row index of the first element of subarray
    integer,    intent(in)    :: jx          !< Col index of the first element of subarray
    integer,    intent(in)    :: xdsc(DLEN_) !< BLACS descriptor for array x
    integer,    intent(out)   :: ierr        !< Error indicator


    !--
    !-- Local variable
    !--
    integer                         :: nprow, npcol, myprow, mypcol
    integer                         :: npiv
    integer,            allocatable :: ipiv(:)
    character(len=128)              :: buf
    integer                         :: itmp(1)
    complex(8)                      :: ztmp(1)
    integer                         :: lzwork, liwork
    integer,            allocatable :: iwork(:)
    complex(8),         allocatable :: zwork(:)


    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


    !--
    !-- Check argument
    !--
    if( n < 1                  ) call MPIX_Abort( PROMPT // " ERROR: n < 1.",                  ierr )
    if( ix < 1                 ) call MPIX_Abort( PROMPT // " ERROR: ix < 1.",                 ierr )
    if( jx < 1                 ) call MPIX_Abort( PROMPT // " ERROR: jx < 1.",                 ierr )
    if( xdsc(CTXT_) /= ctxt    ) call MPIX_Abort( PROMPT // " ERROR: xdsc(CTXT_) /= ctxt.",    ierr )
    if( xdsc(M_) < ix + n - 1  ) call MPIX_Abort( PROMPT // " ERROR: xdsc(M_) < ix + n - 1.",  ierr )
    if( xdsc(N_) < jx + n - 1  ) call MPIX_Abort( PROMPT // " ERROR: xdsc(N_) < jx + n - 1.",  ierr )
    if( xdsc(MB_) /= xdsc(NB_) ) call MPIX_Abort( PROMPT // " ERROR: xdsc(MB_) /= xdsc(NB_).", ierr )


    !--
    !-- Initialize
    !--
    ierr = 0
    call blacs_gridinfo( ctxt, nprow, npcol, myprow, mypcol )
    npiv = numroc( xdsc(M_), xdsc(MB_), myprow, xdsc(RSRC_), nprow ) + xdsc(MB_)
    allocate( ipiv(npiv) )


    !--
    !-- Execute LU factorization
    !--
#ifdef DEBUG
    if( o ) call MPIX_File_write_shared( fh, '(a,x,"Execute LU factorization.")', PROMPT, ierr )
    call blacs_barrier( ctxt, "All" )
#endif
    call pzgetrf( n, n, x, ix, jx, xdsc, ipiv, ierr )
    if( ierr /= 0 )then
      write( unit=buf, fmt='(a,x,"ERROR: pzgetrf returns error code. ierr =",x,i0)' ) PROMPT, ierr
      call MPIX_Abort( trim( buf ), ierr )
    end if !-- ierr


    !--
    !-- Invert matrix
    !--
#ifdef DEBUG
    if( o ) call MPIX_File_write_shared( fh, '(a,x,"Invert matrix.")', PROMPT, ierr )
    call blacs_barrier( ctxt, "All" )
#endif
    !-- Allocate optimied workspace
    call pzgetri( n, x, ix, jx, xdsc, ipiv, ztmp,  -1,     itmp,  -1,     ierr )
    lzwork = max( 1, int( dble( ztmp(1) ) ) )
    liwork = max( 1, itmp(1) )
    allocate( zwork(lzwork), iwork(liwork) )
    !-- Execute matrix inversion
    call pzgetri( n, x, ix, jx, xdsc, ipiv, zwork, lzwork, iwork, liwork, ierr )
    if( ierr /= 0 )then
      write( unit=buf, fmt='(a,x,"ERROR: pzgetri returns error code. ierr =",x,i0)' ) PROMPT, ierr
      call MPIX_Abort( trim( buf ), ierr )
    end if !-- ierr


    return


  end subroutine set_inversematrix_z_old
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


end module mod_inversematrix
!--
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
