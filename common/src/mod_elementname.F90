

!#define DEBUG


!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!-- mod_elementname
!--
module mod_elementname


  implicit none
  private


  integer, private, parameter :: NMAX = 111


  character(len=2), public, parameter :: ELEMENTNAME(1:NMAX) = &
 &  (/ " H","He","Li","Be"," B"," C"," N"," O"," F","Ne", & !--   1- 10
 &     "Na","Mg","Al","Si"," P"," S","Cl","Ar"," K","Ca", & !--  11- 20
 &     "Sc","Ti"," V","Cr","Mn","Fe","Co","Ni","Cu","Zu", & !--  21- 30
 &     "Ga","Ge","As","Se","Br","Kr","Rb","Sr"," Y","Zr", & !--  31- 40
 &     "Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn", & !--  41- 50
 &     "Sb","Te"," I","Xe","Cs","Ba","La","Ce","Pr","Nd", & !--  51- 60
 &     "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb", & !--  61- 70
 &     "Lu","Hf","Ta"," W","Re","Os","Ir","Pt","Au","Hg", & !--  71- 80
 &     "Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th", & !--  81- 90
 &     "Pa"," U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm", & !--  91-100
 &     "Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds", & !-- 101-110
 &     "Rg" /)


end module mod_elementname
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
