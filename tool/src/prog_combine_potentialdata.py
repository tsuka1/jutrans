#!/bin/env python
# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- prog_combine_potentialdata.py
#--
if __name__ == "__main__":


    #--
    #-- Import module
    #--
    import argparse
    import sys
    import os
    from mod_combine_potentialdata import combine_potentialdata


    #--
    #-- Set parameter
    #--
    PROMPT = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"


    #--
    #-- Parse command option and argument
    #--
    parser = argparse.ArgumentParser( description="Combine potential data." )

    parser.add_argument( "--debug", \
                         dest    = "debug", \
                         action  = "store_true", \
                         default = False, \
                         help    = "flag for the debug mode. (default: False)" )

    parser.add_argument( "--dir-left", \
                         dest     = "dir_left", \
                         action   = "store", \
                         type     = str, \
                         default  = None, \
                         help     = "Directory path for potential data to be added to the left." )

    parser.add_argument( "--dir-center", \
                         dest     = "dir_center", \
                         action   = "store", \
                         type     = str, \
                         required = True, \
                         help     = "Directory path for potential data of scattering region." )

    parser.add_argument( "--dir-right", \
                         dest     = "dir_right", \
                         action   = "store", \
                         type     = str, \
                         default  = None, \
                         help     = "Directory path for potential data to be added to the right." )

    parser.add_argument( "--dir-export", \
                         dest     = "dir_export", \
                         action   = "store", \
                         type     = str, \
                         default  = "./", \
                         help     = "path to the param file. (default: ./)" )

    #print parser.parse_args()
    args = parser.parse_args()


    #--
    #-- Display option and argument
    #--
    print( "%s args.dir_left   = %s" % ( PROMPT, args.dir_left )   )
    print( "%s args.dir_center = %s" % ( PROMPT, args.dir_center ) )
    print( "%s args.dir_right  = %s" % ( PROMPT, args.dir_right )  )
    print( "%s args.dir_export = %s" % ( PROMPT, args.dir_export ) )
    print( "%s args.debug      = %s" % ( PROMPT, args.debug )      )


    try:
        ierr = combine_potentialdata( args.dir_center, args.dir_left, args.dir_right, args.dir_export, args.debug )
    except Exception, errmsg:
        sys.exit( errmsg )


    sys.exit( 0 )
