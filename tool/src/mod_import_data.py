# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#-- 
#-- import_data
#--
def import_data( filename, section, debug=False ):
    """ Python function for importing data from info data file set
    
    The python function receives the filename of information file (configure file) and name of the 
    section in the information file, and return the value contained for the section. The data may 
    be included in the information file itself or referring the other data file, which is specified 
    in the information file as an entry.
    
    Args:
        filename: string containing the name of information filename to be processed.
        section: string containing the name of the section.
        debug: bool flag for debug mode.
    
    Returns:
        The python function returns the data corresponding to the section and information file you 
        specified. It could be a scalar, list, and numpy array.
    
    """


    #--
    #-- Import module
    #--
    import os, numpy, sys
    from mod_import_info import import_info


    #--
    #-- Parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Display dummy argument
    #--
    if debug:
        print( "%s filename = %s" % ( PROMPT, filename ) )
        print( "%s section  = %s" % ( PROMPT, section )  )


    #--
    #-- Import infomation file
    #--
    try:
        info = import_info( filename, section )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Check if data is stored in the other file or this file
    #--
    if hasattr( info, "datafilename" ):


        #--
        #-- Get data file name & check existance
        #--
        datafile = info.datafilename
        if not os.path.exists( datafile ):
            raise Exception( "ERROR: data file not found: %s" % datafile )


        #--
        #-- Get offset bytes for data
        #--
        dataoffset = int( info.dataoffsetbyte ) + int( info.recordmarkbyte )


        #--
        #-- Get type of variable
        #--
        variabletype = info.variabletype
        if not variabletype in ( "integer", "real(8)", "complex(8)" ):
            raise Exception( "%s ERROR: .not. variabletype in ( integer, real(8), complex(8) )" % PROMPT )


        #--
        #-- Get data size in byte & in count
        #--
        databyte = int( info.databyte )
        if variabletype == "integer":
            if databyte % 4 != 0:
                raise Exception( "%s ERROR: databyte % 4 != 0" % PROMPT )
            datacnt = databyte / 4
            datatyp = numpy.int32
        elif variabletype == "real(8)":
            if databyte % 8 != 0:
                raise Exception( "%s ERROR: databyte % 8 != 0" % PROMPT )
            datacnt = databyte / 8
            datatyp = numpy.float64
        elif variabletype == "complex(8)":
            if databyte % 16 != 0:
                raise Exception( "%s ERROR: databyte % 16 != 0" % PROMPT )
            datacnt = databyte / 16
            datatyp = numpy.complex128
        else:
            raise Exception( "%s ERROR: .not. variabletype in ( integer, real(8), complex(8) )" % PROMPT )


        #--
        #-- Get data dimension
        #--
        datadim = int( info.variabledim )
        datashape = map( int, info.variableshape.split() )


        #--
        #-- Get byteorder of data
        #--
        databyteorder = info.databyteorder
        if databyteorder.lower() != sys.byteorder + "_endian":
            raise Exception( "%s ERROR: databyteorder != sys.byteorder: sys.byteorder = %s" % ( PROMPT, sys.byteorder ) )


        #--
        #-- Get data
        #--
        datafiletype = info.datafiletype
        if datafiletype in ( "unformatted", "binary" ):
            with open( datafile, "rb" ) as fp:
                fp.seek( dataoffset )
                data = numpy.reshape( numpy.fromfile( fp, datatyp, datacnt ), datashape, order="F" )
        else:
            raise Exception( "%s ERROR: only unformatted data can be precessed" % PROMPT )


    else:
        

        #--
        #-- Get type of variable
        #--
        variabletype = info.variabletype
        if not variabletype in ( "integer", "real(8)", "complex(8)" ):
            raise Exception( "%s ERROR: .not. variabletype in ( integer, real(8), complex(8) )" % PROMPT )


        #--
        #-- Get data type
        #--
        if variabletype == "integer":
            datatyp = numpy.int32
        elif variabletype == "real(8)":
            datatyp = numpy.float64
        elif variabletype == "complex(8)":
            datatyp = numpy.complex128
        else:
            raise Exception( "%s ERROR: .not. variabletype in ( integer, real(8), complex(8) )" % PROMPT )


        #--
        #-- Get data dimension
        #--
        datadim = int( info.variabledim )
        if hasattr( info, "variableshape" ):
            datashape = map( int, info.variableshape.split() )
        else:
            datashape = ( 1, )


        #--
        #-- Get data
        #--
        if datadim == 0:
            if datatyp == numpy.int32:
                data = int( info.value )
            if datatyp == numpy.float64:
                data = float( info.value )
            if datatyp == numpy.complex128:
                raise Exception( "%s ERROR: datadim == 0 and datatyp == complex128 is not yet implemented" % PROMPT )
        else:
            raise Exception( "%s ERROR: datadim != 0 is not yet implemented" % PROMPT )
   

        if debug:
            print( "%s data = %s" % ( PROMPT, data ) )


    return data
#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#-- 
if __name__ == "__main__":

    import sys

    import_data( sys.argv[1], sys.argv[2] )

#-- 
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
