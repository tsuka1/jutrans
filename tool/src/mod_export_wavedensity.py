#!/bin/env python
# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- mod_export_wavedensity.py
#--
def export_wavedensity( fileinfo, fileparam, along_z=False, logscale=False, periodic=False, wavefunction=False, debug=False ):
    """ Python script for export charge density distribution for 3D visualization

    Arguments:
        fileinfo:     meta-data file for wave function(s)
        fileparam:    parameter file exported by kukan8
        along_z:      switch to output linear density integrated over xy planes
        logscale:     switch to output log-scaled densities in xsf file
        periodic:     switch to output periodic field data for xsf fileinfo
        wavefunction: switch to output wave function in two xsf files for real and imaginary parts
        debug:        switch for debug-mode

    """


    #--
    #-- Import module
    #--
    import os.path
    import numpy
    from mod_import_data       import import_data
    from mod_param_f           import param_f
    from mod_export_datatable  import export_datatable
    from mod_export_xcrysden   import export_xcrysden


    #--
    #-- Set parameter
    #--
    PROMPT        = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"
    BOHR2ANGSTROM = 0.529177


    #--
    #-- Display option and argument
    #--
    if debug:
        print( "%s fileinfo  = %s" % ( PROMPT, fileinfo )  )
        print( "%s fileparam = %s" % ( PROMPT, fileparam ) )
        print( "%s along_z   = %s" % ( PROMPT, along_z )   )
        print( "%s logscale  = %s" % ( PROMPT, logscale )  )
        print( "%s periodic  = %s" % ( PROMPT, periodic )  )
        print( "%s debug     = %s" % ( PROMPT, debug )     )


    #--
    #-- Import parameter file & set basis lattice parameter
    #--
    print( "%s Import parameter file. fileparam = %s" % ( PROMPT, fileparam ) )
    if not os.path.isfile( fileparam ):
        raise Exception( "%s ERROR: not os.path.isfile( fileparam ). fileparam = %s" % ( PROMPT, fileparam ) )
    try:
        param = param_f( fileparam, debug )
    except Exception, errmsg:
        raise Exception( errmsg )
    if debug:
        print( "%s XMAX = %f" % ( PROMPT, float( param.XMAX ) ) )
        print( "%s YMAX = %f" % ( PROMPT, float( param.YMAX ) ) )
        print( "%s ZMAX = %f" % ( PROMPT, float( param.ZMAX ) ) )
    lx  = 2.0 * float( param.XMAX ) * BOHR2ANGSTROM
    ly  = 2.0 * float( param.YMAX ) * BOHR2ANGSTROM
    lz  = 2.0 * float( param.ZMAX ) * BOHR2ANGSTROM
    nx  = 2 * int( param.NXMAX )
    ny  = 2 * int( param.NYMAX )
    nz  = 2 * int( param.NZMAX )
    dx  = lx / nx
    dy  = ly / ny
    dz  = lz / nz
    xx  = [ ( i + 0.5 ) * dx - 0.5 * lx for i in xrange( nx ) ]
    yy  = [ ( i + 0.5 ) * dy - 0.5 * ly for i in xrange( ny ) ]
    zz  = [ ( i + 0.5 ) * dz - 0.5 * lz for i in xrange( nz ) ]
    if debug:
        print( "%s ( lx, ly, lz ) = ( %f, %f, %f )" % ( PROMPT, lx, ly, lz ) )
        print( "%s ( nx, ny, nz ) = ( %d, %d, %d )" % ( PROMPT, nx, ny, nz ) )
        print( "%s ( dx, dy, dz ) = ( %f, %f, %f )" % ( PROMPT, dx, dy, dz ) )


    #--
    #-- Import wavefunction & check the array dimensions
    #--
    print( "%s Import wavefunction. fileinfo = %s" % ( PROMPT, fileinfo ) )
    if not os.path.isfile( fileinfo ):
        raise Exception( "%s ERROR: not os.path.isfile( fileinfo ). fileinfo = %s" % ( PROMPT, fileinfo ) )
    try:
        psi = import_data( fileinfo, "wavefunction" )
    except Exception, errmsg:
        raise Exception( errmsg )
    if debug:
        print( "%s psi.shape      = %s" % ( PROMPT, psi.shape ) )
        print( "%s psi.dtype.name = %s" % ( PROMPT, psi.dtype.name ) )
    if psi.ndim != 4:
        raise Exception( "%s ERROR: psi.ndim != 4, psi.ndim = %d." % ( PROMPT, psi.ndim) )
    if psi.shape[0] != nx:
        raise Exception( "%s ERROR: psi.shape[0] != nx. psi.shape[0] = %d, nx = %d." % ( PROMPT, psi.shape[0], nx) )
    if psi.shape[1] != ny:
        raise Exception( "%s ERROR: psi.shape[1] != ny. psi.shape[1] = %d, ny = %d." % ( PROMPT, psi.shape[1], ny) )
    if psi.shape[2] != nz:
        raise Exception( "%s ERROR: psi.shape[2] != nz. psi.shape[2] = %d, nz = %d." % ( PROMPT, psi.shape[2], nz) )


    #--
    #-- Export charge density integrated over xy planes
    #--
    if along_z:
        print( "%s Export charge density integrated over xy planes." % ( PROMPT, ) )
        filename = os.path.splitext( os.path.split( fileinfo )[1] )[0] + "_rho_z.dat"
        rhoz = numpy.sum( abs( psi ) ** 2 / BOHR2ANGSTROM ** 3, axis=( 0, 1 ) ) * dx * dy
        table = numpy.zeros( ( rhoz.shape[0], rhoz.shape[1] + 2 ), dtype=numpy.float64, order="F" )
        table[:,0]  = zz[:]
        table[:,1]  = rhoz.sum( axis=1 )
        table[:,2:] = rhoz[:,:]
        labels = [ "z(Angstrom)", "Total" ] + [ "Ch%2.2d" % ( i + 1, ) for i in xrange( psi.shape[3] ) ]
        try:
            export_datatable( filename, table, labels, debug )
        except Exception, errmsg:
            raise Exception( errmsg )
        return 0


    #--
    #-- Export charge density in XCrysDen xsf format
    #--
    print( "%s Export charge density in XCrysDen xsf format." % ( PROMPT, ) )
    if periodic:
        wf = numpy.zeros( ( psi.shape[0] + 1, psi.shape[1] + 1, psi.shape[2], psi.shape[3] ), dtype=numpy.complex128, order="F" )
        for iy, jy in enumerate( [ i % psi.shape[1] for i in xrange( wf.shape[1] ) ] ):
            for ix, jx in enumerate( [ i % psi.shape[0] for i in xrange( wf.shape[0] ) ] ):
                wf[ix,iy,:,:] = ( psi[jx-1,jy-1,:,:] + psi[jx,jy-1,:,:] + psi[jx-1,jy,  :,:] + psi[jx,jy,  :,:] ) / 4.0
        ( ax, ay, az ) = ( xx[-1] - xx[0] + dx, yy[-1] - yy[0] + dy, zz[-1] - zz[0] )
        ( ox, oy, oz ) = ( xx[0] - 0.5 * dx,    yy[0] - 0.5 * dy,    zz[0]          )
    else:
        wf   = psi
        ( ax, ay, az ) = ( xx[-1] - xx[0],      yy[-1] - yy[0],      zz[-1] - zz[0] )
        ( ox, oy, oz ) = ( xx[0],               yy[0],               zz[0]          )
    #if logscale:
    #    rho = numpy.log10( wf / BOHR2ANGSTROM ** 3 )
    #    basename = os.path.splitext( os.path.split( fileinfo )[1] )[0] + "_log_rho"
    #else:
    #    rho = wf / BOHR2ANGSTROM ** 3
    #    basename = os.path.splitext( os.path.split( fileinfo )[1] )[0] + "_rho"
    comment = "length_in_[Ang.]_and charge_density_in_[/Ang.^3]"
    if wavefunction:
        wf_re = wf.real / BOHR2ANGSTROM ** 1.5
        wf_im = wf.imag / BOHR2ANGSTROM ** 1.5
        basename_re = os.path.splitext( os.path.split( fileinfo )[1] )[0] + "_RE"
        basename_im = os.path.splitext( os.path.split( fileinfo )[1] )[0] + "_IM"
        try:
            export_xcrysden( wf_re, basename_re, ax, ay, az, ox, oy, oz, comment, debug )
            export_xcrysden( wf_im, basename_im, ax, ay, az, ox, oy, oz, comment, debug )
        except Exception, errmsg:
            raise Exception( errmsg )
    else:
        rho = abs( wf ) ** 2 / BOHR2ANGSTROM ** 3
        basename = os.path.splitext( os.path.split( fileinfo )[1] )[0] + "_rho"
        try:
            export_xcrysden( rho, basename, ax, ay, az, ox, oy, oz, comment, debug )
        except Exception, errmsg:
            raise Exception( errmsg )


    return 0


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#--
#--
if __name__ == "__main__":


    import sys


    fileparam = sys.argv[1]
    fileinfo  = sys.argv[2]


    try:
        ierr = export_wavedensity( fileinfo, fileparam, debug=True )
    except Exception, errmsg:
        sys.exit( errmsg )


    sys.exit( ierr )


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
