# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- construct_projectors
#--
def construct_projectors( dir_exe, debug=False ):
    """ Python function for constructing PAW projector data. 

    The python function receives the path of the directory containing the PAW projector data set, 
    and automatically seek and read the necessary data to construct PAW projectors in the real
    space domain.

    Args:
        dir_exe: The path of the directory containing necessary PAW projector data files.
        debug: Flag for debug mode

    Returns:
        The PAW projector data is returned as the array, which contains numpy.arrays of PAW 
        projector data over a real-space grids for each atom.
    """


    #--
    #-- Import module
    #--
    import os.path
    import numpy

    import mod_param_f
    import mod_import_data


    #--
    #-- Set parameter
    #--
    PROMPT           = __name__.upper() + " >>>"
    DIR_EXE          = dir_exe if dir_exe[-1] == "/" else dir_exe + "/"
    FILE_PARAM       = DIR_EXE + "param.f"
    FILE_NPRJ        = DIR_EXE + "nprj.info"
    FILE_LSTNLMX     = DIR_EXE + "lstnlmx.info"
    FILE_LSTNL       = DIR_EXE + "lstnl.info"
    FILE_VNLCP       = DIR_EXE + "vnlcp.info"


    #--
    #-- Display dummy argument
    #--
    if debug:
        print( "%s dir_exe = %s" % ( PROMPT, dir_exe ) )
        print( "%s debug   = %s" % ( PROMPT, debug )   )


    #--
    #-- Check dir and files
    #--
    if debug:
        print( "%s DIR_EXE      = %s" % ( PROMPT, DIR_EXE )      )
        print( "%s FILE_PARAM   = %s" % ( PROMPT, FILE_PARAM )   )
        print( "%s FILE_NPRJ    = %s" % ( PROMPT, FILE_NPRJ )    )
        print( "%s FILE_LSTNLMX = %s" % ( PROMPT, FILE_LSTNLMX ) )
        print( "%s FILE_LSTNL   = %s" % ( PROMPT, FILE_LSTNL )   )
        print( "%s FILE_VNLCP   = %s" % ( PROMPT, FILE_VNLCP )   )
    if not os.path.isdir( DIR_EXE ):
        raise Exception( "%s ERROR: not os.path.isdir( DIR_EXE ). DIR_EXE = %s" % ( PROMPT, DIR_EXE ) )
    if not os.path.isfile( FILE_PARAM ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_PARAM ). FILE_PARAM = %s" % ( PROMPT, FILE_PARAM ) )
    if not os.path.isfile( FILE_NPRJ ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_NPRJ ). FILE_NPRJ = %s" % ( PROMPT, FILE_NPRJ ) )
    if not os.path.isfile( FILE_LSTNLMX ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_LSTNLMX ). FILE_LSTNLMX = %s" % ( PROMPT, FILE_LSTNLMX ) )
    if not os.path.isfile( FILE_LSTNL ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_LSTNL ). FILE_LSTNL = %s" % ( PROMPT, FILE_LSTNL ) )
    if not os.path.isfile( FILE_VNLCP ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_VNLCP ). FILE_VNLCP = %s" % ( PROMPT, FILE_VNLCP ) )


    #--
    #-- Import FILE_PARAM
    #--
    print( "%s Import FILE_PARAM" % ( PROMPT ) )
    param = mod_param_f.param_f( FILE_PARAM )
    if debug:
        print( "%s param.__dict__ = %s" % ( PROMPT, param.__dict__ ) )


    #--
    #-- Set number of atom to be processed
    #--
    XMAX   = float( param.XMAX )
    YMAX   = float( param.YMAX )
    ZMAX   = float( param.ZMAX )
    NXMAX  = int( param.NXMAX )
    NYMAX  = int( param.NYMAX )
    NZMAX  = int( param.NZMAX )
    NPXMAX = int( param.NPXMAX )
    NPYMAX = int( param.NPYMAX )
    NPZMAX = int( param.NPZMAX )
    NATOM  = int( param.NATOM )
    NUMS   = int( param.NUMS )
    NPRJMX = int( param.NPRJMX )
#    NF     = int( param.NF )
#    FERM   = float( param.FERM )
    if debug:
        print( "%s XMAX    = %f" % ( PROMPT, XMAX    ) )
        print( "%s YMAX    = %f" % ( PROMPT, YMAX    ) )
        print( "%s ZMAX    = %f" % ( PROMPT, ZMAX    ) )
        print( "%s NXMAX   = %d" % ( PROMPT, NXMAX   ) )
        print( "%s NYMAX   = %d" % ( PROMPT, NYMAX   ) )
        print( "%s NZMAX   = %d" % ( PROMPT, NZMAX   ) )
        print( "%s NPXMAX  = %d" % ( PROMPT, NPXMAX  ) )
        print( "%s NPYMAX  = %d" % ( PROMPT, NPYMAX  ) )
        print( "%s NPZMAX  = %d" % ( PROMPT, NPZMAX  ) )
        print( "%s NATOM   = %d" % ( PROMPT, NATOM   ) )
#        print( "%s NUMS    = %d" % ( PROMPT, NUMS    ) )
        print( "%s NPRJMX  = %d" % ( PROMPT, NPRJMX  ) )
#        print( "%s NF      = %d" % ( PROMPT, NF      ) )
#        print( "%s FERM    = %f" % ( PROMPT, FERM    ) )
               
               
    #--        
    #-- Import FILE_NPRJ
    #--        
    print( "%s Import FILE_NPRJ" % ( PROMPT ) )
    nprj = mod_import_data.import_data( FILE_NPRJ, "nprj", debug )
    if debug:  
        print( "%s nprj.shape = %s" % ( PROMPT, nprj.shape ) )
        print( "%s nprj       = %s" % ( PROMPT, nprj )       )
    if nprj.shape[0] != NATOM:
        raise Exception( "%s ERROR: nprj.shape[0] != NATOM. nprj.shape[0] = %d NATOM = %d" % ( PROMPT, nprj.shape[0], NATOM ) )
               
               
    #--
    #-- Import FILE_LSTNLMX
    #--
    print( "%s Import FILE_LSTNLMX" % ( PROMPT ) )
    lstnlmx = mod_import_data.import_data( FILE_LSTNLMX, "lstnlmx", debug )
    if debug:
        print( "%s lstnlmx.shape = %s" % ( PROMPT, lstnlmx.shape ) )
        print( "%s lstnlmx       = %s" % ( PROMPT, lstnlmx )       )
    if lstnlmx.shape[0] != NATOM:
        raise Exception( "%s ERROR: lstnlmx.shape[0] != NATOM. lstnlmx.shape[0] = %d NATOM = %d" % ( PROMPT, lstnlmx.shape[0], NATOM ) )


    #--
    #-- Import FILE_LSTNL
    #--
    print( "%s Import FILE_LSTNL" % ( PROMPT ) )
    lstnl = mod_import_data.import_data( FILE_LSTNL, "lstnl", debug )
    if debug:
        print( "%s lstnl.shape = %s" % ( PROMPT, lstnl.shape ) )
    if lstnl.shape[0] != 8 * NPXMAX * NPYMAX * NPZMAX:
        raise Exception( "%s ERROR: lstnl.shape[0] != 8 * NPXMAX * NPYMAX * NPZMAX. lstnl.shape[0] = %d NPXMAX = %d NPYMAX = %d NPZMAX = %d" % ( PROMPT, lstnl.shape[0], NPXMAX, NPYMAX, NPZMAX ) )
    if lstnl.shape[1] != NATOM:
        raise Exception( "%s ERROR: lstnl.shape[1] != NATOM. lstnl.shape[1] = %d NATOM = %d" % ( PROMPT, lstnl.shape[1], NATOM ) )


    #--
    #-- Import FILE_VNLCP
    #--
    print( "%s Import FILE_VNLCP" % ( PROMPT ) )
    vnlcp = mod_import_data.import_data( FILE_VNLCP, "vnlcp", debug )
    if debug:
        print( "%s vnlcp.shape = %s" % ( PROMPT, vnlcp.shape ) )
    if vnlcp.shape[0] != 8 * NPXMAX * NPYMAX * NPZMAX:
        raise Exception( "%s ERROR: vnlcp.shape[0] != 8 * NPXMAX * NPYMAX * NPZMAX. vnlcp.shape[0] = %d NPXMAX = %d NPYMAX = %d NPZMAX = %d" % ( PROMPT, vnlcp.shape[0], NPXMAX, NPYMAX, NPZMAX ) )
    if vnlcp.shape[1] != NPRJMX:
        raise Exception( "%s ERROR: vnlcp.shape[1] != NPRJMX. vnlcp.shape[1] = %d NPRJMX = %d" % ( PROMPT, vnlcp.shape[2], NPRJMX ) )
    if vnlcp.shape[2] != NATOM:
        raise Exception( "%s ERROR: vnlcp.shape[2] != NATOM. vnlcp.shape[2] = %d NATOM = %d" % ( PROMPT, vnlcp.shape[2], NATOM ) )


    #--
    #-- Construct list of projectors over the real-space domain
    #--
    print( "%s Construct list of projectors over the real-space domain" % ( PROMPT ) )
    projectors = []
    dxyz = XMAX * YMAX * ZMAX / ( NXMAX * NYMAX * NZMAX )
    for ia in xrange( NATOM ):
        buf = numpy.zeros( ( 8 * NXMAX * NYMAX * NZMAX, nprj[ia] ), dtype=numpy.float64, order="F" )
        for i in xrange( lstnlmx[ia] ):
            indx = lstnl[i,ia] - 1
            buf[indx,0:nprj[ia]] = vnlcp[i,0:nprj[ia],ia] / dxyz #-- Remove DXYZ component
        if debug:
            print( "%s iatom = %4d buf.shape = %s" % ( PROMPT, ia + 1, buf.shape ) )
        projectors.append( buf.reshape( ( 2 * NXMAX, 2 * NYMAX, 2 * NZMAX, nprj[ia] ), order="F" ) )


    return projectors


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


if __name__ == "__main__":

    import sys
    
    
    try:
        projs = construct_projectors( sys.argv[1], debug=True )
    except Exception, errmsg:
        sys.exit( errmsg )

    print len( projs )
    for proj in projs:
        print proj.shape

    sys.exit( 0 )


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
