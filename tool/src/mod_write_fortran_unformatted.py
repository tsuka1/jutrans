#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- write_fortran_unformatted()
#--
def write_fortran_unformatted( filename, data, write_type=None, reclength=4, byteswap=False ):


    #--
    #-- Import module
    #--
    import numpy


    #--
    #-- Parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Display dummy argument
    #--
    print( "%s filename   = %s" % ( PROMPT, filename )   )
    print( "%s write_type = %s" % ( PROMPT, write_type ) )
    print( "%s reclength  = %d" % ( PROMPT, reclength )  )
    print( "%s byteswap   = %s" % ( PROMPT, byteswap )   )
    if not reclength in ( 4, 8 ):
        raise Exception, "%s ERROR: not reclength in ( 4, 8 )" % PROMPT


    #--
    #-- Prepare databody
    #--
    if write_type == None:
        databody = data.ravel( order="F" )
    else:
        databody = data.astype( write_type ).ravel( order="F" )


    #--
    #-- Prepare recordmarker
    #--
    databyte = numpy.array( databody.nbytes )
    if reclength == 4:
        recordmark = databyte.astype( numpy.int32 )
    elif reclength == 8:
        recordmark = databyte.astype( numpy.int64 )


    #-- 
    #-- Swap byteorder
    #--
    if byteswap:
        recordmark = recordmark.byteswap()
        databody   = databody.byteswap()


    #--
    #-- Export data
    #--
    with open( filename, "wb" ) as fp:
        recordmark.tofile( fp )
        databody.tofile( fp )
        recordmark.tofile( fp )


    return True
#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
if __name__ == "__main__":

    import numpy

    test = [ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 ]
    test = numpy.reshape( test, ( 3, 3 ), order="F" )
    print test
    print test.ravel( order="F" )
    write_fortran_unformatted( "./test.data", test )
#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
