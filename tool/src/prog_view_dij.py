#!/bin/env python
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- prog_view_dij
#--
if __name__ == "__main__":

    #--
    #-- Set parameter
    #--
    PROMPT           = __name__.upper() + " >>>"
    FILE_DIJ         = "dij.info"


    #--
    #-- Import module
    #--
    import sys, copy, numpy, math
    from argparse           import ArgumentParser
    from mod_import_data    import import_data


    #--
    #-- Construct parser instance
    #--
    desc = u"{0} [Args] [Options]\nDetailed options -h or --help".format(__file__)
    parser = ArgumentParser(description=desc)


    #--
    #-- Set command options
    #--
    parser.add_argument( "--src", type=str, dest="dir_src", required=True, help="source directory" )

    parser.add_argument( "--eps", type=float, dest="epsilon", default=0.0, help="negligible value" )


    #--
    #-- Parse command option
    #--
    args = parser.parse_args()
    if args.dir_src[-1] != "/": args.dir_src += "/" #-- correct directory name
    print( "%s dir_src = %s" % ( PROMPT, args.dir_src ) )
    print( "%s epsilon = %e" % ( PROMPT, args.epsilon ) )


    #--
    #-- Import dij
    #--
    filename = args.dir_src + FILE_DIJ
    try:
        dij = import_data( filename, "dij" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Finalize
    #--
    for iatom in xrange( dij.shape[3] ):
        for ispin in xrange( dij.shape[2] ):
            print( "%s iatom = %d ispin = %d" % ( PROMPT, iatom + 1, ispin + 1 ) )
            str = " dij"
            for i in xrange( dij.shape[0] ):
                str += "%8d" % ( i + 1 )
            print( "%s %s" % ( PROMPT, str ) )
            for j in xrange( dij.shape[1] ):
                str = "%4d" % ( j + 1 )
                for i in xrange( dij.shape[0] ):
                    if abs( dij[i,j,ispin,iatom] ) > args.epsilon:
                        str += "%8.0e" % ( dij[i,j,ispin,iatom] )
                    else:
                        str += " -------"
                print( "%s %s" % ( PROMPT, str ) )
    sys.exit( 0 )


#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
