# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- operate_overlapmatrix
#--
def operate_overlapmatrix( param, wfcs, sijs, projs, debug=False ):


    #--
    #-- Import module
    #--
    import numpy
    from copy import deepcopy


    #--
    #-- Set parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Display dummy argumen
    #--
    if debug:
        print( "%s len( param.__dict__ ) = %d" % ( PROMPT, len( param.__dict__ ) ) )
        print( "%s wfcs.shape            = %s" % ( PROMPT, wfcs.shape )            )
        print( "%s len( sijs )           = %s" % ( PROMPT, len( sijs ) )           )
        print( "%s len( projs )          = %s" % ( PROMPT, len( projs ) )          )
        print( "%s debug                 = %s" % ( PROMPT, debug )                 )


    #--
    #-- Chack array sizes
    #--
    if debug:
        print( "%s Chack array sizes" % ( PROMPT ) )
    nx = 2 * int( param.NXMAX )
    ny = 2 * int( param.NYMAX )
    nz = 2 * int( param.NZMAX )
    if debug:
        print( "%s ( nx, ny, nz ) = ( %d, %d, %d )" % ( PROMPT, nx, ny, nz ) )
    natom = int( param.NATOM )
    if wfcs.ndim == 3:
        vecs = wfcs[:,:,:,numpy.newaxis]
        nvec = 1
    elif wfcs.ndim == 4:
        vecs = wfcs
        nvec = vecs.shape[3]
    else:
        raise Exception( "%s wfcs.ndim must be 3 or 4. wfcs.ndim = %d" % ( PROMPT, wfcs.ndim ) )
    if vecs.shape != ( nx, ny, nz, nvec ):
        raise Exception( "%s vecs.shape != ( nx, ny, nz, nvec ).  vecs.shape = %s nx = %d ny = %d nz = %d nvec = %d" % ( PROMPT, vecs.shape, nx, ny, nz, nvec ) )
    if len( sijs ) != natom:
        raise Exception( "%s len( sijs ) != natom. len( sijs ) = %d natom = %d" % ( PROMPT, len( sijs ), natom ) )
    if len( projs ) != natom:
        raise Exception( "%s len( projs ) != natom. len( projs ) = %d natom = %d" % ( PROMPT, len( projs ), natom ) )
    for i, ( sij, proj ) in enumerate( zip( sijs, projs ) ):
        if proj.shape != ( nx, ny, nz, sij.shape[0] ):
            raise Exception( "%s proj.shape != ( nx, ny, nz, sij.shape[0] ). iatom = %d proj.shape = %s nx = %d ny = %d nz = %d sij.shape[0] = %d" % ( PROMPT, i + 1, proj.shape, nx, ny, nz, sij.shape[0] ) )


    #--
    #-- Operate sij matrix on the vector
    #-- |z> = ( 1 + \sum_{atom,i,j} |p_i> sij <p_j| ) * |vec>
    #--
    if debug:
        print( "%s Operate sij matrix on the vector" % ( PROMPT ) )
    dx = float( param.XMAX) / int( param.NXMAX )
    dy = float( param.YMAX) / int( param.NYMAX )
    dz = float( param.ZMAX) / int( param.NZMAX )
    if debug:
        print( "%s ( dx, dy, dz ) = ( %f, %f, %f )" % ( PROMPT, dx, dy, dz ) )
    dxyz = dx *  dy * dz
    v = numpy.matrix( vecs.reshape( ( nx * ny * nz , nvec ), order="F" ) )
    z = deepcopy( v )
    for i, ( sij, proj ) in enumerate( zip( sijs, projs ) ):
        nprj = sij.shape[0]
        s = numpy.matrix( sij )
        p = numpy.matrix( proj.reshape( ( nx * ny * nz, nprj ), order="F" ) )
        a = p * s
        b = p.T * v * dxyz
        z += ( a * b )
    if debug:
        print( "%s z.shape = %s" % ( PROMPT, z.shape ) )
        for i in xrange( z.shape[1] ):
            print( "%s i = %3d, <vec|1+S|vec> = %9.6f" % ( PROMPT, i + 1, v[:,i].T * z[:,i] * dxyz ) )


#    bra = numpy.reshape( numpy.array( v ), ( nx, ny, nz, nvec ), order="F" )
#    ket = numpy.reshape( numpy.array( z ), ( nx, ny, nz, nvec ), order="F" )
#    for i in xrange( nvec ):
#        print( "%s i = %3d, <vec|1+S|vec> = %9.6f" % ( PROMPT, i + 1, dxyz * numpy.sum( bra[:,:,:,i] * ket[:,:,:,i] ) ) )
#        print( "%s i = %3d, <vec|1+S|vec> = %9.6f" % ( PROMPT, i + 1, dxyz * numpy.sum( vecs[:,:,:,i] * ket[:,:,:,i] ) ) )
#        print( "%s i = %3d, <vec|1+S|vec> = %9.6f" % ( PROMPT, i + 1, dxyz * numpy.sum( wfcs[:,:,:,i] * ket[:,:,:,i] ) ) )


    return numpy.reshape( numpy.array( z ), ( nx, ny, nz, nvec ), order="F" )





