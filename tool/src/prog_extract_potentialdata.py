#!/bin/env python
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- prog_extract_potentialdata
#--
if __name__ == "__main__":

    #--
    #-- Set parameter
    #--
    PROMPT           = __name__.upper() + " >>>"
    FILE_PARAMF      = "param.f"
    FILE_ATOMXYZ     = "atom.xyz"
    FILE_VLOCAL      = "vlocal.info"
    FILE_NPRJ        = "nprj.info"
    FILE_LSTNLMX     = "lstnlmx.info"
    FILE_DIJ         = "dij.info"
    FILE_SIJ         = "sij.info"
    FILE_LSTNL       = "lstnl.info"
    FILE_VNLCP       = "vnlcp.info"


    #--
    #-- Import module
    #--
    import sys, copy, numpy, math, os.path
    from argparse           import ArgumentParser
    from mod_param_f        import param_f
    from mod_import_atomxyz import import_atomxyz
    from mod_import_data    import import_data
    from mod_export_param_f import export_param_f
    from mod_export_atomxyz import export_atomxyz
    from mod_export_data    import export_data


    #--
    #-- Construct parser instance
    #--
    desc = u"{0} [Args] [Options]\nDetailed options -h or --help".format(__file__)
    parser = ArgumentParser(description=desc)


    #--
    #-- Set command options
    #--
    parser.add_argument( "--src",   type=str,   dest="dir_src", required=True,                 help="source directory" )
    parser.add_argument( "--dst",   type=str,   dest="dir_dst", required=True,                 help="output directory" )
    parser.add_argument( "--izstr", type=int,   dest="izstr",   required=True,                 help="iz index from which extract starts." )
    parser.add_argument( "--izend", type=int,   dest="izend",   required=True,                 help="iz index from which extract ends." )

    parser.add_argument( "--eps", type=float, dest="epsilon", default=0.0, help="Tolerance in z direction" )

    parser.add_argument( "--debug", dest="debug", action="store_true", default=False, help="Debug mode" )


    #--
    #-- Parse command option
    #--
    args = parser.parse_args()
    if args.dir_src[-1] != "/": args.dir_src += "/" #-- correct directory name
    if args.dir_dst[-1] != "/": args.dir_dst += "/" #-- correct directory name
    print( "%s dir_src = %s" % ( PROMPT, args.dir_src ) )
    print( "%s dir_dst = %s" % ( PROMPT, args.dir_dst ) )
    print( "%s izstr   = %d" % ( PROMPT, args.izstr )   )
    print( "%s izend   = %d" % ( PROMPT, args.izend )   )
    print( "%s epsilon = %e" % ( PROMPT, args.epsilon ) )
    if args.dir_src == args.dir_dst:             sys.exit( "%s ERROR: dir_src == dir_dst"     % PROMPT )
    if args.izstr < 1 or args.izend < 1:         sys.exit( "%s ERROR: izstr < 1 or izend < 1" % PROMPT )
    if args.izstr == args.izend:                 sys.exit( "%s ERROR: izstr == izend"         % PROMPT )
    crossboundary = True if args.izend < args.izstr else False


    #--
    #-- Import parameter file
    #--
    print( "%s Import parameter file" % PROMPT )
    filename = args.dir_src + FILE_PARAMF
    try:
        param = param_f( filename )
    except Exception as err:
        sys.exit( err )
    if args.izstr > 2 * int( param.NZMAX ): sys.exit( "%s ERROR: izstr > 2 * NZMAX" % PROMPT )
    if args.izend > 2 * int( param.NZMAX ): sys.exit( "%s ERROR: izend > 2 * NZMAX" % PROMPT )
    #-- 
    if crossboundary:
        list_iz_ext = range( args.izstr, 2 * int( param.NZMAX ) + 1 ) + range( 1, args.izend + 1 )
    else:
        list_iz_ext = range( args.izstr, args.izend + 1 )
    nz_ext = len( list_iz_ext )
    print( "%s izstr = %d izend = %d nz_ext = %d" % ( PROMPT, args.izstr, args.izend, nz_ext ) )
    if nz_ext % 2 == 1: sys.exit( "%s ERROR: nz_ext %% 2 == 1" % PROMPT )


    #--
    #-- Import atomic coordinate
    #--
    print( "%s Import atomic coordinate" % PROMPT )
    filename = args.dir_src + FILE_ATOMXYZ
    natom    = int( param.NATOM )
    try:
        atoms = import_atomxyz( filename, natom )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Import local potential
    #--
    print( "%s Import local potential" % PROMPT )
    filename = args.dir_src + FILE_VLOCAL
    try:
        vlocal = import_data( filename, "vlocal" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Import numbers of projectors
    #--
    print( "%s Import numbers of projectors" % PROMPT )
    filename = args.dir_src + FILE_NPRJ
    try:
        nprj = import_data( filename, "nprj" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Import numbers of grid points in the atomic sphere
    #--
    print( "%s Import numbers of grid points in the atomic sphere" % PROMPT )
    filename = args.dir_src + FILE_LSTNLMX
    try:
        lstnlmx = import_data( filename, "lstnlmx" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Import dij
    #--
    print( "%s Import dij" % PROMPT )
    filename = args.dir_src + FILE_DIJ
    try:
        dij = import_data( filename, "dij" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Import sij
    #--
    print( "%s Import sij" % PROMPT )
    filename = args.dir_src + FILE_SIJ
    try:
        sij = import_data( filename, "sij" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Import lstnl
    #--
    print( "%s Import lstnl" % PROMPT )
    filename = args.dir_src + FILE_LSTNL
    try:
        lstnl = import_data( filename, "lstnl" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Import vnlcp
    #--
    print( "%s Import vnlcp" % PROMPT )
    filename = args.dir_src + FILE_VNLCP
    try:
        vnlcp = import_data( filename, "vnlcp" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Create list of atoms, which are extracted
    #--
    print( "%s Create list of atoms, which are extracted" % PROMPT )
    dz   = float( param.ZMAX ) / int( param.NZMAX )
    list_atoms_ext  = []
    for iz in list_iz_ext:
        zstr = ( iz - 1 ) * dz - float( param.ZMAX )
        zend =   iz       * dz - float( param.ZMAX )
        for i, atom in enumerate( atoms ):
            if zstr + args.epsilon < atom.z <= zend + args.epsilon:
                list_atoms_ext.append( i )
    for i, atom in enumerate( atoms ):
        str = "i = %4d nz = %4d %s x = %12.8f y = %12.8f z = %12.8f" % ( i + 1, atoms[i].nz, atoms[i].name, atoms[i].x, atoms[i].y, atoms[i].z )
        if i in list_atoms_ext:
            str += " <---Extract---"
        print( "%s %s" % ( PROMPT, str ) )


    #--
    #-- Export param_ext
    #--
    print( "%s Export param_ext" % PROMPT )
    param_ext = copy.deepcopy( param )
    dz   = float( param.ZMAX ) / int( param.NZMAX )
    zstr = ( args.izstr - 1 ) * dz - float( param.ZMAX )
    zend =   args.izend       * dz - float( param.ZMAX )
    if crossboundary:
        param_ext.ZMAX  = ( ( float( param.ZMAX ) - zstr ) + ( zend - ( -float( param.ZMAX ) ) ) ) / 2
        param_ext.NZMAX = nz_ext / 2
        param_ext.NATOM = len( list_atoms_ext )
    else:
        param_ext.ZMAX  = ( zend - zstr ) / 2.0
        param_ext.NZMAX = nz_ext / 2
        param_ext.NATOM = len( list_atoms_ext )
    print( "%s param_ext.ZMAX  = %.16f" % ( PROMPT, param_ext.ZMAX )  )
    print( "%s param_ext.NZMAX = %d"    % ( PROMPT, param_ext.NZMAX ) )
    print( "%s param_ext.NATOM = %d"    % ( PROMPT, param_ext.NATOM ) )
    filename = args.dir_dst + FILE_PARAMF
    try:
        export_param_f( param_ext, filename )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Export atoms_ext
    #--
    print( "%s Export atoms_ext" % PROMPT )
    atoms_ext = []
    lattice_ext = [ 0.0, 0.0, 0.0 ]
    for i in list_atoms_ext:
        atoms_ext.append( copy.deepcopy( atoms[i] ) )
    lattice_ext[0] = 2.0 * float( param.XMAX )
    lattice_ext[1] = 2.0 * float( param.YMAX )
    dz   = float( param.ZMAX ) / int( param.NZMAX )
    zstr = ( args.izstr - 1 ) * dz - float( param.ZMAX )
    zend =   args.izend       * dz - float( param.ZMAX )
    if crossboundary:
        zshift = -( zstr + zend + 2.0 * float( param.ZMAX ) ) / 2.0
        for i, atom in enumerate( atoms_ext ):
            if atom.z < zend + args.epsilon:
                atom.z = atom.z + 2.0 * float( param.ZMAX ) + zshift
            else:
                atom.z = atom.z + zshift
        lattice_ext[2] = 2.0 * float( param.ZMAX ) + zend - zstr
    else:
        zshift = -( zstr + zend ) / 2.0
        for i, atom in enumerate( atoms_ext ):
            atom.z = atom.z + zshift
        lattice_ext[2] = zend - zstr
    for i, atom in enumerate( atoms_ext ):
        print( "%s i = %4d nz = %4d %s x = %12.8f y = %12.8f z = %12.8f" % (PROMPT, i + 1, atom.nz, atom.name, atom.x, atom.y, atom.z ) )
    filename = os.path.join( args.dir_dst, FILE_ATOMXYZ )
    try:
        export_atomxyz( filename, atoms_ext, lattice_ext )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Export vlocal_ext
    #--
    print( "%s Export vlocal_ext" % PROMPT )
    if crossboundary:
        vlocal_ext_shape = ( vlocal.shape[0], vlocal.shape[1], nz_ext, vlocal.shape[3] )
        vlocal_ext = numpy.zeros( vlocal_ext_shape, dtype=numpy.float64, order="F" )
        iz0 = 2 * int( param.NZMAX ) - args.izstr + 1
        iz1 = 2 * int( param.NZMAX )
        vlocal_ext[:,:,0:iz0,:]      = copy.deepcopy( vlocal[:,:,args.izstr-1:iz1,:] )
        vlocal_ext[:,:,iz0:nz_ext,:] = copy.deepcopy( vlocal[:,:,0:args.izend,:] )
    else:
        vlocal_ext = copy.deepcopy( vlocal[:,:,args.izstr-1:args.izend,:] )
    try:
        export_data( vlocal_ext, args.dir_dst, "vlocal.info", "vlocal.data", "vlocal", "unformatted" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Export nprj_ext
    #--
    print( "%s Export nprj_ext" % PROMPT )
    nprj_ext = numpy.zeros( ( len( list_atoms_ext ), ), dtype=numpy.int32, order="F" )
    for i, ia in enumerate( list_atoms_ext ):
        nprj_ext[i] = nprj[ia]
    try:
        export_data( nprj_ext, args.dir_dst, "nprj.info", "nprj.data", "nprj", "unformatted" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Export lstnlmx_ext
    #--
    print( "%s Export lstnlmx_ext" % PROMPT )
    lstnlmx_ext = numpy.zeros( ( len( list_atoms_ext ), ), dtype=numpy.int32, order="F" )
    for i, ia in enumerate( list_atoms_ext ):
        lstnlmx_ext[i] = lstnlmx[ia]
    try:
        export_data( lstnlmx_ext, args.dir_dst, "lstnlmx.info", "lstnlmx.data", "lstnlmx", "unformatted" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Export dij_ext
    #--
    print( "%s Export dij_ext" % PROMPT )
    shape = ( dij.shape[0], dij.shape[1], dij.shape[2], len( list_atoms_ext ) )
    dij_ext = numpy.zeros( shape, dtype=numpy.float64, order="F" )
    for i, ia in enumerate( list_atoms_ext ):
        dij_ext[:,:,:,i] = dij[:,:,:,ia]
    try:
        export_data( dij_ext, args.dir_dst, "dij.info", "dij.data", "dij", "unformatted" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Export sij_ext
    #--
    print( "%s Export sij_ext" % PROMPT )
    shape = ( sij.shape[0], sij.shape[1], len( list_atoms_ext ) )
    sij_ext = numpy.zeros( shape, dtype=numpy.float64, order="F" )
    for i, ia in enumerate( list_atoms_ext ):
        sij_ext[:,:,i] = sij[:,:,ia]
    try:
        export_data( sij_ext, args.dir_dst, "sij.info", "sij.data", "sij", "unformatted" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Construct lstnl_ext
    #--
    print( "%s Construct lstnl_ext" % PROMPT )
    shape = ( lstnl.shape[0], len( list_atoms_ext ) )
    lstnl_ext = numpy.zeros( shape, dtype=numpy.int32, order="F" )
    nxy    = 4 * int( param.NXMAX ) * int( param.NYMAX )
    dz     = float( param.ZMAX ) / int( param.NZMAX )
    zmax   = float( param.ZMAX )
    zstr = ( args.izstr - 1 ) * dz - float( param.ZMAX )
    zend =   args.izend       * dz - float( param.ZMAX )
    lz     = 2.0 * float( param.ZMAX )
    nz     = 2 * int( param.NZMAX )
    for i, ia in enumerate( list_atoms_ext ):
        atom = atoms[ia]
        for j in xrange( lstnlmx[ia] ):
            iz  = ( lstnl[j,ia] - 1 ) // nxy + 1  #-- Z index in the ORIGINAL cell
            ixy = ( lstnl[j,ia] - 1 ) %  nxy + 1  #-- XY index in the ORIGINAL cell
            zz = ( iz - 0.5 ) * dz - zmax         #-- Z coordinate in the ORIGINAL cell
            #-- Absolute index with respect to the ORIGINAL cell ( negative index number may appear )
            if crossboundary:
                if atom.z < zend + args.epsilon:
                    iz  = iz - int( math.floor( ( zz - atom.z ) / lz + 0.5 ) ) * nz + nz
                else:
                    iz  = iz - int( math.floor( ( zz - atom.z ) / lz + 0.5 ) ) * nz
            else:
                iz  = iz - int( math.floor( ( zz - atom.z ) / lz + 0.5 ) ) * nz
            jz  = iz - args.izstr + 1
            jz  = ( jz - 1 ) % nz_ext + 1
            jxy = ixy
            lstnl_ext[j,i] = ( jz - 1 ) * nxy + jxy


    #--
    #-- Check lstnl_ext
    #--
    print( "%s Check lstnl_ext" % PROMPT )
    nxy_ext  = 4 * int( param_ext.NXMAX ) * int( param_ext.NYMAX )
    dz_ext   = float( param_ext.ZMAX ) / int( param_ext.NZMAX )
    zmax_ext = float( param_ext.ZMAX )
    lz_ext   = 2.0 * float( param_ext.ZMAX )
    nz_ext   = 2 * int( param_ext.NZMAX )
    for i, ia in enumerate( list_atoms_ext ):
        atom_ext = atoms_ext[i]
        izmin = nz_ext
        izmax = 1
        for j in xrange( lstnlmx_ext[i] ):
            iz  = ( lstnl_ext[j,i] - 1 ) // nxy_ext + 1  #-- Z index in the EXTRACTED cell
            ixy = ( lstnl_ext[j,i] - 1 ) %  nxy_ext + 1  #-- XY index in the EXTRACTED cell
            zz = ( iz - 0.5 ) * dz_ext - zmax_ext        #-- Z coordinate in the EXTRACTED cell
            #-- Absolute index with respect to the EXTRACTED cell ( negative index number may appear )
            iz  = iz - int( math.floor( ( zz - atom_ext.z ) / lz_ext + 0.5 ) ) * nz_ext
            izmin = min( izmin, iz )
            izmax = max( izmax, iz )
            izext = izmax - izmin + 1
        print( "%s i = %4d, ia = %4d, z = %8.4f, izmin = %4d, izmax = %4d, range = %4d" % ( PROMPT, i + 1, ia + 1, atom_ext.z, izmin, izmax, izext ) )
        if( izmax - izmin + 1 > nz_ext ):
            sys.exit( "izmax - izmin + 1 > nz_ext" )


    #--
    #-- Export lstnl_ext
    #--
    print( "%s Export lstnl_ext" % PROMPT )
    try:
        export_data( lstnl_ext, args.dir_dst, "lstnl.info", "lstnl.data", "lstnl", "unformatted" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Export vnlcp_ext
    #--
    print( "%s Export vnlcp_ext" % PROMPT )
    shape = ( vnlcp.shape[0], vnlcp.shape[1], len( list_atoms_ext ) )
    vnlcp_ext = numpy.zeros( shape, dtype=numpy.float64, order="F" )
    for i, ia in enumerate( list_atoms_ext ):
        vnlcp_ext[:,:,i] = vnlcp[:,:,ia]
    try:
        export_data( vnlcp_ext, args.dir_dst, "vnlcp.info", "vnlcp.data", "vnlcp", "unformatted" )
    except Exception as err:
        sys.exit( err )


    #--
    #-- Finalize
    #--
    print( "%s Finalize" % PROMPT )
    for i, atom in enumerate( atoms ):
        str = "i = %4d nz = %4d %s x = %12.8f y = %12.8f z = %12.8f" % ( i + 1, atoms[i].nz, atoms[i].name, atoms[i].x, atoms[i].y, atoms[i].z )
        if i in list_atoms_ext:
            str += " <---Extract---"
        print( "%s %s" % ( PROMPT, str ) )
    sys.exit( 0 )


#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
