# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- decompose_wavefunction()
#--
def decompose_wavefunction( dir_basis, file_info, dir_export="./", ispin=0, ikpt=0, debug=False ):


    #--
    #-- Import module
    #--
    import os
    import sys
    import numpy
    import mod_import_data
    import mod_param_f
    import mod_construct_projectors
    import mod_construct_sij
    import mod_operate_overlapmatrix
    import mod_export_datatable


    #--
    #-- Parameter
    #--
    PROMPT = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"


    #--
    #-- Display dummy argument
    #--
    if debug:
        print( "%s dir_basis   = %s" % ( PROMPT, dir_basis )   )
        print( "%s file_info   = %s" % ( PROMPT, file_info )   )
        print( "%s dir_export  = %s" % ( PROMPT, dir_export )  )
        print( "%s ispin       = %d" % ( PROMPT, ispin )       )
        print( "%s ikpt        = %d" % ( PROMPT, ikpt )        )
        print( "%s debug       = %s" % ( PROMPT, debug )       )


    #--
    #-- Correct/Chack directory/file names
    #--
    if not os.path.isdir( dir_basis ):
        raise Exception( "%s ERROR: not os.path.isdir( dir_kukan ). dir_basis = %s" % ( PROMPT, dir_basis ) )
    if not os.path.isfile( file_info ):
        raise Exception( "%s ERROR: not os.path.isfile( file_info ). file_info = %s" % ( PROMPT, file_info ) )
    if not os.path.isdir( dir_export ):
        raise Exception( "%s ERROR: not os.path.isdir( dir_export ). dir_export = %s" % ( PROMPT, dir_export ) )


    #--
    #-- Import scattering wave function
    #--
    print( "%s Import scattering wave function." % PROMPT )
    try:
        wav = mod_import_data.import_data( file_info, "wavefunction" )
    except Exception, errmsg:
        raise Exception( errmsg )
    if debug:
        print( "%s wave.shape = %s" % ( PROMPT, wav.shape ) )



    #--
    #-- Import dir_basis/param.f
    #--
    print( "%s Import dir_basis/param.f." % PROMPT )
    try:
        param = mod_param_f.param_f( os.path.join( dir_basis, "param.f" ) )
    except Exception, errmsg:
        raise Exception( errmsg )
    if debug:
        print( "%s param.__dict__ = %s" % ( PROMPT, param.__dict__ ) )
    lx = 2 * float( param.XMAX )
    ly = 2 * float( param.YMAX )
    lz = 2 * float( param.ZMAX )
    dx = float( param.XMAX ) / int( param.NXMAX )
    dy = float( param.YMAX ) / int( param.NYMAX )
    dz = float( param.ZMAX ) / int( param.NZMAX )
    if debug:
        print( "%s lx = %f, ly = %f, lz = %f dx = %f, dy = %f, dz = %f" % ( PROMPT, lx, ly, lz, dx, dy, dz ) )
    dxyz = dx * dy * dz


    #--
    #-- Import dir_basis/wfc.(info|data)
    #--
    print( "%s Import dir_basis/wfc.(info|data)." % PROMPT )
    try:
        wfc = mod_import_data.import_data( os.path.join( dir_basis, "wfc.info" ), "wavefunction" )
    except Exception, errmsg:
        raise Exception( errmsg )
    if debug:
        print( "%s wfc.shape = %s" % ( PROMPT, wfc.shape ) )
    wfc = wfc[:,:,:,:,ispin,ikpt]
    if wfc.shape[0] != wav.shape[0]:
        raise Exception( "%s ERROR: wfc.shape[0] /= wav.shape[0]. wfc.shape[0] = %d, wav.shape[0] =%d." % ( PROMPT, wfc.shape[0], wav.shape[0] ) )
    if wfc.shape[1] != wav.shape[1]:
        raise Exception( "%s ERROR: wfc.shape[1] /= wav.shape[1]. wfc.shape[1] = %d, wav.shape[1] =%d." % ( PROMPT, wfc.shape[1], wav.shape[1] ) )
    if wfc.shape[2] != wav.shape[2]:
        raise Exception( "%s ERROR: wfc.shape[2] /= wav.shape[2]. wfc.shape[2] = %d, wav.shape[2] =%d." % ( PROMPT, wfc.shape[2], wav.shape[2] ) )
    if debug:
        for iband in xrange( wfc.shape[3] ):
            print( "%s iband = %4d, <wfc|wfc> = %9.6f" % ( PROMPT, iband + 1, numpy.sum( numpy.square( wfc[:,:,:,iband ] ) ) * dxyz ) )


    #--
    #-- Import kukan PAW projectors
    #--
    print( "%s Import kukan PAW projectors" % PROMPT )
    try:
        prjs = mod_construct_projectors.construct_projectors( dir_basis, debug=False )
    except Exception, errmsg:
        raise Exception( errmsg )
    for ia, prj in enumerate( prjs ):
        if prj.shape[0] != wav.shape[0]:
            raise Exception( "%s ERROR: prj.shape[0] /= wav.shape[0]. prj.shape[0] = %d, wav.shape[0] =%d." % ( PROMPT, prj.shape[0], wav.shape[0] ) )
        if prj.shape[1] != wav.shape[1]:
            raise Exception( "%s ERROR: prj.shape[1] /= wav.shape[1]. prj.shape[1] = %d, wav.shape[1] =%d." % ( PROMPT, prj.shape[1], wav.shape[1] ) )
        if prj.shape[2] != wav.shape[2]:
            raise Exception( "%s ERROR: prj.shape[2] /= wav.shape[2]. prj.shape[2] = %d, wav.shape[2] =%d." % ( PROMPT, prj.shape[2], wav.shape[2] ) )


    #--
    #-- Import kukan sij matrix
    #--
    print( "%s Import kukan sij matrix" % PROMPT )
    try:
        sijs = mod_construct_sij.construct_sij( dir_basis, debug=False )
    except Exception, errmsg:
        raise Exception( errmsg )
    buf = mod_operate_overlapmatrix.operate_overlapmatrix( param, wfc, sijs, prjs, debug=False )
    if debug:
        for i in xrange( buf.shape[3] ):
            print( "%s i = %4d, <wfc|1+S|wfc> = %9.6f" % ( PROMPT, i + 1, dxyz * numpy.sum( wfc[:,:,:,i] * buf[:,:,:,i] ) ) )


    #--
    #-- Operate kukan overlap matrix to juTrans scatterin wave functions
    #--
    print( "%s Operate kukan overlap matrix to juTrans scatterin wave functions" % PROMPT )
    try:
        vec = mod_operate_overlapmatrix.operate_overlapmatrix( param, wav[:,:,:,:], sijs, prjs, debug=False )
    except Exception, errmsg:
        raise Exception( errmsg )
    if debug:
        print( "%s vec.shape = %s" % ( PROMPT, vec.shape ) )
        for i in xrange( vec.shape[3] ):
            print( "%s i = %4d, <wav|1+S|wav> = %s" % ( PROMPT, i + 1, dxyz * numpy.sum( wav[:,:,:,i] * vec[:,:,:,i] ) ) )


    #--
    #-- Evaluate basis population
    #--
    print( "%s Evaluate basis population" % PROMPT )
    nxyz = wfc.shape[0] * wfc.shape[1] * wfc.shape[2]
    nwfc = wfc.shape[3]
    nxyz = vec.shape[0] * vec.shape[1] * vec.shape[2]
    nvec = vec.shape[3]
    a = dxyz * numpy.matrix( wfc.reshape( ( nxyz, nwfc ), order="F" ) ).T * numpy.matrix( vec.reshape( ( nxyz, nvec ), order="F" ) )
    pops = numpy.square( numpy.absolute( a ) )
    if debug:
        print( "%s pops.shape = %s" % ( PROMPT, pops.shape ) )
        for j in xrange( pops.shape[1] ):
            str = "%s j = %3d" % ( PROMPT, j + 1 )
            for i in xrange( pops.shape[0] ):
                str += ", %9.2e" % ( pops[i,j], )
            print( "%s" % ( str, ) )


    #--
    #-- Export data
    #--
    filename = os.path.join( dir_export, os.path.splitext( os.path.basename( file_info ) )[0] + "_decompose.dat" )
    print( "%s filename = %s" % ( PROMPT, filename ) )
    data = numpy.zeros( ( pops.shape[0], pops.shape[1] + 2 ), dtype=numpy.float64, order="F" )
    if debug:
        print( "%s data.shape = %s" % ( PROMPT, data.shape ) )
    data[:,0]   = range( 1, pops.shape[0] + 1 )
    data[:,1:2] = numpy.sum( pops, axis=1 )[:,0:1]
    data[:,2:]  = pops[:,:]
    label = [ "index", "Total" ] + [ "Ch%3.3d" % ( j + 1, ) for j in xrange( pops.shape[1] ) ]
    try:
        mod_export_datatable.export_datatable( filename, data, label, debug )
    except Exception, errmsg:
        raise Exception( errmsg )


    return pops


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


if __name__ == "__main__":


    import os
    import sys


    dir_home   = os.getenv("HOME")
    dir_basis  = os.path.join( dir_home, "data/PtChain_H2Molecule/kukan8e_data" )
    file_info  = os.path.join( dir_home, "data/PtChain_H2Impurity/juOBM_TEST_0/eigenchannel_ie0001is1.info" )
    dir_export = "./"


    try:
        a = decompose_wavefunction( dir_basis, file_info, dir_export, debug=True )
    except Exception, errmsg:
        sys.exit( errmsg )


    sys.exit( 0 )

