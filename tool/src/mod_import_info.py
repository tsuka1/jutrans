#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#-- 
#-- import_info
#--
def import_info( filename, section, debug=False ):


    #--
    #-- Import module
    #--
    import ConfigParser, os
    from mod_trans_dict2class import trans_dict2class


    #--
    #-- Parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Display dummy argument
    #--
    if debug:
        print "%s filename = %s" % ( PROMPT, filename )
        print "%s section  = %s" % ( PROMPT, section  )


    #--
    #-- Import infomation file & set 
    #--
    conf = ConfigParser.SafeConfigParser() #-- Construct 
    if os.path.exists( filename ):
        conf.read( filename )
    else:
        raise Exception( "ERROR: infomation file not found: %s" % filename )


    #--
    #-- Check if section exists
    #--
    if not section in conf.sections():
        raise Exception( "ERROR: section not found: %s" % section )


    #--
    #-- Construct dictionary of option-value pairs
    #--
    dict = {}
    for option, value in conf.items( section ):
        if option == "datafilename":
            dict[ option ] = os.path.join( os.path.split( filename )[0], value )
        else:
            dict[ option ] = value
        if debug:
            print "%s %s\t=\t%s" % ( PROMPT, option, dict[ option ] )


    return trans_dict2class( dict )
#-- 
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#-- 
if __name__ == "__main__":

    import sys

    import_info( sys.argv[1], sys.argv[2] )

#-- 
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
