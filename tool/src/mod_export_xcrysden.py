# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#-- 
#-- mod_export_xcrysden()
#--
#-- 2017.03.07 change PROMPT
#-- 2015.11.04 remove white spaces from comment string
#-- 2015.11.04 debug option is added to dummy arguments
#-- 2014.06.17 3D and 4D are both available for input
#-- 2014.06.17 Change ox, oy, and oz to be optional argument
#-- 2014.06.17 Move the positions of ox, oy, oz to the end of argumens
#-- 2014.04.09 Created first
#--
def export_xcrysden( a, basename, lx, ly, lz, ox=0.0, oy=0.0, oz=0.0, comment="comment_line", debug=False ):


    #--
    #-- Import module
    #--
    import os.path
    import numpy


    #--
    #-- Parameter
    #--
    PROMPT = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"
    CHUNK  = 8


    #--
    #-- Display dummy argument
    #--
    if debug:
        print( "%s a.shape        = %s"             % ( PROMPT, a.shape )    )
        print( "%s basename       = %s"             % ( PROMPT, basename )   )
        print( "%s ( lx, ly, lz ) = ( %f, %f, %f )" % ( PROMPT, lx, ly, lz ) )
        print( "%s ( ox, oy, oz ) = ( %f, %f, %f )" % ( PROMPT, ox, oy, oz ) )
        print( "%s comment        = %s"             % ( PROMPT, comment )    )


    #--
    #-- Check array dimension ( 3 and 4 are available )
    #--
    if a.ndim == 3:
        ( nx, ny, nz ) = a.shape
        nn             = 1
        a = a.reshape( ( nx, ny, nz, nn ), order="F" )
    elif a.ndim == 4:
        ( nx, ny, nz, nn ) = a.shape
    else:
        raise Exception( "ERROR: invalid array dimension" )
    if debug:
        print( "%s a.shape      = %s" % ( PROMPT, a.shape )      )
        print( "%s a.dtype.name = %s" % ( PROMPT, a.dtype.name ) )


    #--
    #-- Replace white brank in comment to underbar
    #--
    comment = comment.replace( " ", "_" )
    if debug:
        print( "%s comment = %s" % ( PROMPT, comment ) )



    #--
    #-- Format and output string
    #--
    for ii in xrange( nn ):
        str  = "BEGIN_BLOCK_DATAGRID_3D\n"
        str += "  %s\n" % comment
        str += "  BEGIN_DATAGRID_3D_data1\n"
        str += "    %d  %d  %d\n" % ( nx, ny, nz )
        str += "    %f  %f  %f\n" % ( ox, oy, oz )
        str += "    %f  %f  %f\n" % ( lx, 0.0, 0.0 )
        str += "    %f  %f  %f\n" % ( 0.0, ly, 0.0 )
        str += "    %f  %f  %f\n" % ( 0.0, 0.0, lz )
        b = a[:,:,:,ii].reshape( ( nx * ny * nz, ), order="F" )
        for i, val in enumerate( b ):
            str += "  " if i % CHUNK == 0 else ""
            str += "  %13.6e" % val
            str += "\n" if i % CHUNK == CHUNK - 1 or i == len( b ) - 1 else ""

        str += "  END_DATAGRID_3D\n"
        str += "END_BLOCK_DATAGRID_3D\n"

        filename = "%s_%3.3d.xsf" % ( basename, ii + 1 )
        if debug:
            print( "%s filename = %s" % ( PROMPT, filename ) )
        with open( filename, "w" ) as fp:
            fp.write( str )


    return 0

#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0




#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
if __name__ == "__main__":


    import sys, numpy, math


    a = numpy.empty( ( 10, 10, 10, 2 ), dtype=numpy.float64, order="F" )
    for m in xrange( a.shape[3] ):
        for k in xrange( a.shape[2] ):
            for j in xrange( a.shape[1] ):
                for i in xrange( a.shape[0] ):
                    a[i,j,k,m] = 10.0 / math.sqrt( ( i - 3.3 * ( m + 1 ) ) ** 2 + ( j - 3.3 * ( m + 1 ) ) ** 2 + ( k - 3.3 * ( m + 1 ) ) ** 2 )


    try:
        return_code = export_xcrysden( a, "test", 1.0, 1.0, 1.0, comment="comment comment", debug=True )
    except Exception, errmsg:
        sys.exit( errmsg )


#    print return_code
    sys.exit( 0 )

#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
