#!/bin/env python
# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- prog_decompose_wavefunction.py
#--
if __name__ == "__main__":


    #--
    #-- Import module
    #--
    import argparse
    import sys
    import os
    import numpy
    from mod_decompose_wavefunction import decompose_wavefunction


    #--
    #-- Set parameter
    #--
    PROMPT = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"


    #--
    #-- Parse command option and argument
    #--
    parser = argparse.ArgumentParser( description="Decompose wave functions into atomic/molecular orbitals." )

    parser.add_argument( "--debug", \
                         dest    = "debug", \
                         action  = "store_true", \
                         default = False, \
                         help    = "flag for the debug mode. (default: False)" )

    parser.add_argument( "--dir-basis", \
                         dest     = "dir_basis", \
                         action   = "store", \
                         type     = str, \
                         required = True, \
                         help     = "Directory path to basis wave functions." )

    parser.add_argument( "--file-info", \
                         dest     = "file_info", \
                         action   = "store", \
                         type     = str, \
                         required = True, \
                         help     = "path to the information file containing wave functions." )

    parser.add_argument( "--dir-export", \
                         dest     = "dir_export", \
                         action   = "store", \
                         type     = str, \
                         default  = "./", \
                         help     = "path to the param file. (default: ./)" )

    parser.add_argument( "--ispin", \
                         dest     = "ispin", \
                         action   = "store", \
                         type     = int, \
                         choices  = [ 0, 1 ], \
                         default  = 0, \
                         help     = "Index of spin of basis wave functions. (default: 0)" )

    parser.add_argument( "--ikpt", \
                         dest     = "ikpt", \
                         action   = "store", \
                         type     = int, \
                         default  = 0, \
                         help     = "Index of k-point of basis wave functions. (default: 0)" )

    #print parser.parse_args()
    args = parser.parse_args()


    #--
    #-- Display option and argument
    #--
    print( "%s args.dir_basis  = %s" % ( PROMPT, args.dir_basis )  )
    print( "%s args.file_info  = %s" % ( PROMPT, args.file_info )  )
    print( "%s args.dir_export = %s" % ( PROMPT, args.dir_export ) )
    print( "%s args.ispin      = %d" % ( PROMPT, args.ispin )      )
    print( "%s args.ikpt       = %d" % ( PROMPT, args.ikpt )       )
    print( "%s args.debug      = %s" % ( PROMPT, args.debug )      )


    try:
        pops = decompose_wavefunction( args.dir_basis, args.file_info, args.dir_export, args.ispin, args.ikpt, args.debug )
    except Exception, errmsg:
        #print( "%s %s" % ( PROMPT, errmsg ) )
        sys.exit( errmsg )

    
    print( "%s pops.shape = %s" % ( PROMPT, pops.shape ) )
    for j in xrange( pops.shape[1] ):
        str = "%s %s wav(%3d)" % ( PROMPT, os.path.basename( args.file_info ), j + 1 )
        for i in xrange( pops.shape[0] ):
            str += ", %9.2e" % ( pops[i,j], )
        print( "%s" % ( str, ) )
    str = "%s %s wav(Tot)" % ( PROMPT, os.path.basename( args.file_info ) )
    for i in xrange( pops.shape[0] ):
        str += ", %9.2e" % ( numpy.sum( pops[i,:], axis=1 ), )
    print( "%s" % ( str, ) )


    sys.exit( 0 )
  
