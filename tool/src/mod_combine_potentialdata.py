#!/bin/env python
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- mod_combine_potentialdata
#--
def combine_potentialdata( dir_center, dir_left=None, dir_right=None, dir_export="./", debug=False ):


    #--
    #-- Import module
    #--
    import os, copy, numpy, math
    from mod_param_f        import param_f
    from mod_export_param_f import export_param_f
    from mod_import_atomxyz import import_atomxyz
    from mod_export_atomxyz import export_atomxyz
    from mod_import_data    import import_data
    from mod_export_data    import export_data


    #--
    #-- Set parameter
    #--
    PROMPT       = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"
    FILE_PARAMF  = "param.f"
    FILE_ATOMXYZ = "atom.xyz"
    FILE_VLOCAL  = "vlocal.info"
    FILE_NPRJ    = "nprj.info"
    FILE_LSTNLMX = "lstnlmx.info"
    FILE_DIJ     = "dij.info"
    FILE_SIJ     = "sij.info"
    FILE_LSTNL   = "lstnl.info"
    FILE_VNLCP   = "vnlcp.info"
    EPS          = 1.0e-10


    #--
    #-- Display dummy argument
    #--
    if debug:
        print( "%s dir_center = %s" % ( PROMPT, dir_center ) )
        print( "%s dir_left   = %s" % ( PROMPT, dir_left )   )
        print( "%s dir_right  = %s" % ( PROMPT, dir_right )  )
        print( "%s dir_export = %s" % ( PROMPT, dir_export ) )
        print( "%s debug      = %s" % ( PROMPT, debug )      )


    #--
    #--
    #--
    left  = dir_left  != None
    right = dir_right != None
    if not left and not right:
        raise Exception( "%s ERROR: dir_left and dir_right are both None." % ( PROMPT, ) )


    #--
    #-- Check directories
    #--
    if not os.path.isdir( dir_center ):
        raise Exception( "%s ERROR: directory not found. dir_center = %s" % ( PROMPT, dir_center ) )
    if left and not os.path.isdir( dir_left ):
        raise Exception( "%s ERROR: directory not found. dir_left = %s" % ( PROMPT, dir_left ) )
    if right and not os.path.isdir( dir_right ):
        raise Exception( "%s ERROR: directory not found. dir_right = %s" % ( PROMPT, dir_right ) )
    if not os.path.isdir( dir_export ):
        raise Exception( "%s ERROR: directory not found. dir_export = %s" % ( PROMPT, dir_export ) )
    if dir_export in ( dir_center, dir_left, dir_right ):
        raise Exception( "%s ERROR: dir_export in ( dir_center, dir_left, dir_right )." % ( PROMPT, ) )


    #--
    #-- Import parameter files param.f
    #--
    print( "%s Import parameter file." % ( PROMPT, ) )
    try:
        param_center = param_f( os.path.join( dir_center, FILE_PARAMF ) )
        param_left   = param_f( os.path.join( dir_left,   FILE_PARAMF ) ) if left  else None
        param_right  = param_f( os.path.join( dir_right,  FILE_PARAMF ) ) if right else None
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s param_left   = %s" % ( PROMPT, param_left )   )
        print( "%s param_center = %s" % ( PROMPT, param_center ) )
        print( "%s param_right  = %s" % ( PROMPT, param_right )  )


    #--
    #-- Check parameters in param_left and param_right
    #--
    print( "%s Check parameters in param_left and param_right." % ( PROMPT, ) )
    if left:
        if param_left.XMAX != param_center.XMAX:
            raise Exception( "%s ERROR: param_left.XMAX != param_center.XMAX." % ( PROMPT, ) )
        if param_left.YMAX != param_center.YMAX:
            raise Exception( "%s ERROR: param_left.YMAX != param_center.YMAX." % ( PROMPT, ) )
        if param_left.NXMAX != param_center.NXMAX:
            raise Exception( "%s ERROR: param_left.NXMAX != param_center.NXMAX." % ( PROMPT, ) )
        if param_left.NYMAX != param_center.NYMAX:
            raise Exception( "%s ERROR: param_left.NYMAX != param_center.NYMAX." % ( PROMPT, ) )
        if abs( float( param_left.ZMAX ) / int( param_left.NZMAX ) - float( param_center.ZMAX ) / int( param_center.NZMAX ) ) > EPS:
            raise Exception( "%s ERROR: param_left.DZ != param_center.DZ." % ( PROMPT, ) )
        if param_left.NF != param_center.NF:
            raise Exception( "%s ERROR: param_left.NF != param_center.NF." % ( PROMPT, ) )
    if right:
        if param_right.XMAX != param_center.XMAX:
            raise Exception( "%s ERROR: param_right.XMAX != param_center.XMAX." % ( PROMPT, ) )
        if param_right.YMAX != param_center.YMAX:
            raise Exception( "%s ERROR: param_right.YMAX != param_center.YMAX." % ( PROMPT, ) )
        if param_right.NXMAX != param_center.NXMAX:
            raise Exception( "%s ERROR: param_right.NXMAX != param_center.NXMAX." % ( PROMPT, ) )
        if param_right.NYMAX != param_center.NYMAX:
            raise Exception( "%s ERROR: param_right.NYMAX != param_center.NYMAX." % ( PROMPT, ) )
        if abs( float( param_right.ZMAX ) / int( param_right.NZMAX ) - float( param_center.ZMAX ) / int( param_center.NZMAX ) ) > EPS:
            raise Exception( "%s ERROR: param_right.DZ != param_center.DZ." % ( PROMPT, ) )
        if param_right.NF != param_center.NF:
            raise Exception( "%s ERROR: param_right.NF != param_center.NF." % ( PROMPT, ) )


    #--
    #-- Export parameter file param.f
    #--
    print( "%s Export parameter file param.f." % ( PROMPT, ) )
    param_export       = copy.deepcopy( param_center )
    lz_export = 2 * float( param_center.ZMAX ) + ( 2 * float( param_left.ZMAX ) if left else 0 ) + ( 2 * float( param_right.ZMAX ) if right else 0 )
    param_export.ZMAX  = 0.5 * lz_export
    nz_export = 2 * int( param_center.NZMAX ) + ( 2 * int( param_left.NZMAX ) if left else 0 ) + ( 2 * int( param_right.NZMAX ) if right else 0 )
    param_export.NZMAX = nz_export / 2
    param_export.NATOM = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 ) + ( int( param_right.NATOM ) if right else 0 )
    param_export.NPXMAX = max( int( param_center.NPXMAX ), int( param_left.NPXMAX ) if left else 0, int( param_right.NPXMAX ) if right else 0 )
    param_export.NPYMAX = max( int( param_center.NPYMAX ), int( param_left.NPYMAX ) if left else 0, int( param_right.NPYMAX ) if right else 0 )
    param_export.NPZMAX = max( int( param_center.NPZMAX ), int( param_left.NPZMAX ) if left else 0, int( param_right.NPZMAX ) if right else 0 )
    print( "%s param_export.ZMAX   = %.16f" % ( PROMPT, param_export.ZMAX )   )
    print( "%s param_export.NZMAX  = %d"    % ( PROMPT, param_export.NZMAX )  )
    print( "%s param_export.NATOM  = %d"    % ( PROMPT, param_export.NATOM )  )
    print( "%s param_export.NPXMAX = %d"    % ( PROMPT, param_export.NPXMAX ) )
    print( "%s param_export.NPYMAX = %d"    % ( PROMPT, param_export.NPYMAX ) )
    print( "%s param_export.NPZMAX = %d"    % ( PROMPT, param_export.NPZMAX ) )
    try:
        export_param_f( param_export, os.path.join( dir_export, FILE_PARAMF ) )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import atomic coordinate atom.xyz
    #--
    print( "%s Import atomic coordinate atom.xyz." % ( PROMPT, ) )
    natom_center = int( param_center.NATOM )
    natom_left   = int( param_left.NATOM   ) if left  else 0
    natom_right  = int( param_right.NATOM  ) if right else 0
    try:
        atoms_center = import_atomxyz( os.path.join( dir_center, FILE_ATOMXYZ ), natom_center )
        atoms_left   = import_atomxyz( os.path.join( dir_left,   FILE_ATOMXYZ ), natom_left   ) if left  else None
        atoms_right  = import_atomxyz( os.path.join( dir_right,  FILE_ATOMXYZ ), natom_right  ) if right else None
    except Exception as err:
        raise Exception( err )


    #--
    #-- Shift atomic positions
    #--
    print( "%s Shift atomic positions." % ( PROMPT, ) )
    zmin = -1 * float( param_center.ZMAX ) - ( 2 * float( param_left.ZMAX  ) if left  else 0 )
    zmax =      float( param_center.ZMAX ) + ( 2 * float( param_right.ZMAX ) if right else 0 )
    zshift_center = 0.5 * ( zmin + zmax )
    zshift_left   = zshift_center - ( float( param_center.ZMAX ) + float( param_left.ZMAX  ) if left  else 0.0 )
    zshift_right  = zshift_center + ( float( param_center.ZMAX ) + float( param_right.ZMAX ) if right else 0.0 )
    if debug:
        print( "%s param_left.ZMAX   = %12.8f, zshift_left   = %12.8f" % ( PROMPT, float( param_left.ZMAX ),   zshift_left )   )
        print( "%s param_center.ZMAX = %12.8f, zshift_center = %12.8f" % ( PROMPT, float( param_center.ZMAX ), zshift_center ) )
        print( "%s param_right.ZMAX  = %12.8f, zshift_right  = %12.8f" % ( PROMPT, float( param_right.ZMAX ),  zshift_right )  )
    atoms_export = []
    if left:
        atoms_export += copy.deepcopy( atoms_left )
        istr = 0
        iend = int( param_left.NATOM )
        for atom in atoms_export[istr:iend]:
            atom.z += zshift_left
    atoms_export += copy.deepcopy( atoms_center )
    istr = int( param_left.NATOM ) if left else 0
    iend = istr + int( param_center.NATOM )
    for atom in atoms_export[istr:iend]:
        atom.z += zshift_center
    if right:
        atoms_export += copy.deepcopy( atoms_right )
        istr = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 )
        iend = istr + int( param_right.NATOM )
        for atom in atoms_export[istr:iend]:
            atom.z += zshift_right
    if debug:
        for ia, atom in enumerate( atoms_export ):
            print( "%s atoms_export: ia = %3d, %s" % ( PROMPT, ia + 1, atom ) )


    #--
    #-- Export atomic coordinate atom.xyz
    #--
    print( "%s Export atomic coordinate atom.xyz." % PROMPT )
    lattice_export = ( 2 * float( param_export.XMAX ), 2 * float( param_export.YMAX ), 2 * float( param_export.ZMAX ) )
    try:
        export_atomxyz( os.path.join( dir_export + FILE_ATOMXYZ ), atoms_export, lattice_export )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import local potential local.data
    #--
    print( "%s Import local potential local.data." % ( PROMPT, ) )
    try:
        vlocal_center = import_data( os.path.join( dir_center, FILE_VLOCAL ), "vlocal" )
        vlocal_left   = import_data( os.path.join( dir_left,   FILE_VLOCAL ), "vlocal" ) if left  else 0
        vlocal_right  = import_data( os.path.join( dir_right,  FILE_VLOCAL ), "vlocal" ) if right else 0
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s vlocal_left.shape   = %s" % ( PROMPT, vlocal_left.shape )   )
        print( "%s vlocal_center.shape = %s" % ( PROMPT, vlocal_center.shape ) )
        print( "%s vlocal_right.shape  = %s" % ( PROMPT, vlocal_right.shape )  )


    #--
    #-- Check shapes of vlocal data
    #--
    print( "%s Check shapes of vlocal data." % ( PROMPT, ) )
    if vlocal_center.shape != ( 2 * int( param_center.NXMAX ), 2 * int( param_center.NYMAX ), 2 * int( param_center.NZMAX ), int( param_center.NUMS ) ):
        raise Exception( "%s ERROR: vlocal_center.shape does not match. vlocal_center = %s" % ( PROMPT, vlocal_center.shape ) )
    if left and vlocal_left.shape != ( 2 * int( param_left.NXMAX ), 2 * int( param_left.NYMAX ), 2 * int( param_left.NZMAX ), int( param_left.NUMS ) ):
        raise Exception( "%s ERROR: vlocal_left.shape does not match. vlocal_left = %s" % ( PROMPT, vlocal_left.shape ) )
    if left and vlocal_right.shape != ( 2 * int( param_right.NXMAX ), 2 * int( param_right.NYMAX ), 2 * int( param_right.NZMAX ), int( param_right.NUMS ) ):
        raise Exception( "%s ERROR: vlocal_right.shape does not match. vlocal_right = %s" % ( PROMPT, vlocal_right.shape ) )


    #--
    #-- Export local potential local.data
    #--
    print( "%s Export local potential local.data." % ( PROMPT, ) )
    vlocal_export = numpy.zeros( ( 2 * int( param_export.NXMAX ), 2 * int( param_export.NYMAX ), 2 * int( param_export.NZMAX ), int( param_export.NUMS ) ), dtype=numpy.float64, order="F" )
    vshift_left  = float( param_center.FERM ) - float( param_left.FERM )
    vshift_right = float( param_center.FERM ) - float( param_right.FERM )
    print( "%s param_center.FERM = %.8f, param_left.FERM  = %.8f, vshift_left  = %.8f" % ( PROMPT, float( param_center.FERM ), float( param_left.FERM ),  vshift_left )  )
    print( "%s param_center.FERM = %.8f, param_right.FERM = %.8f, vshift_right = %.8f" % ( PROMPT, float( param_center.FERM ), float( param_right.FERM ), vshift_right ) )
    if left:
        izstr = 0
        izend = 2 * int( param_left.NZMAX )
        vlocal_export[:,:,izstr:izend,:] = vlocal_left[:,:,:,:] + vshift_left
    izstr = 2 * int( param_left.NZMAX ) if left else 0
    izend = izstr + 2 * int( param_center.NZMAX )
    vlocal_export[:,:,izstr:izend,:] = vlocal_center[:,:,:,:]
    if right:
        izstr = 2 * int( param_center.NZMAX ) + ( 2 * int( param_left.NZMAX ) if left else 0 )
        izend = izstr + 2 * int( param_right.NZMAX )
        vlocal_export[:,:,izstr:izend,:] = vlocal_right[:,:,:,:] + vshift_right
    try:
        export_data( vlocal_export, dir_export, FILE_VLOCAL, FILE_VLOCAL.replace( ".info", ".data" ), FILE_VLOCAL.split( "." )[0], "unformatted" )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import numbers of projectors
    #--
    print( "%s Import numbers of projectors." % PROMPT )
    try:
        nprj_center = import_data( os.path.join( dir_center, FILE_NPRJ ), "nprj" )
        nprj_left   = import_data( os.path.join( dir_left,   FILE_NPRJ ), "nprj" ) if left  else None
        nprj_right  = import_data( os.path.join( dir_right,  FILE_NPRJ ), "nprj" ) if right else None
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s nprj_left   = %s" % ( PROMPT, nprj_left )   )
        print( "%s nprj_center = %s" % ( PROMPT, nprj_center ) )
        print( "%s nprj_right  = %s" % ( PROMPT, nprj_right )  )


    #--
    #-- Export numbers of projectors
    #--
    print( "%s Export numbers of projectors." % PROMPT )
    nprj_export = numpy.zeros( ( int( param_export.NATOM ), ), dtype=numpy.int32, order="F" )
    if left:
        istr = 0
        iend = int( param_left.NATOM )
        nprj_export[istr:iend] = nprj_left[:]
    istr = int( param_left.NATOM ) if left else 0
    iend = istr + int( param_center.NATOM )
    nprj_export[istr:iend] = nprj_center[:]
    if right:
        istr = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 )
        iend = istr + int( param_right.NATOM )
        nprj_export[istr:iend] = nprj_right[:]
    if debug:
        print( "%s nprj_export = %s" % ( PROMPT, nprj_export ) )
    try:
        export_data( nprj_export, dir_export, FILE_NPRJ, FILE_NPRJ.replace( ".info", ".data" ), FILE_NPRJ.split( "." )[0], "unformatted" )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import numbers of grid points in the atomic sphere.
    #--
    print( "%s Import numbers of grid points in the atomic sphere." % PROMPT )
    try:
        lstnlmx_center = import_data( os.path.join( dir_center, FILE_LSTNLMX ), "lstnlmx" )
        lstnlmx_left   = import_data( os.path.join( dir_left,   FILE_LSTNLMX ), "lstnlmx" ) if left  else None
        lstnlmx_right  = import_data( os.path.join( dir_right,  FILE_LSTNLMX ), "lstnlmx" ) if right else None
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s lstnlmx_left   = %s" % ( PROMPT, lstnlmx_left )   )
        print( "%s lstnlmx_center = %s" % ( PROMPT, lstnlmx_center ) )
        print( "%s lstnlmx_right  = %s" % ( PROMPT, lstnlmx_right )  )


    #--
    #-- Export numbers of grid points in the atomic sphere.
    #--
    print( "%s Export numbers of grid points in the atomic sphere." % PROMPT )
    lstnlmx_export = numpy.zeros( ( int( param_export.NATOM ), ), dtype=numpy.int32, order="F" )
    if left:
        istr = 0
        iend = int( param_left.NATOM )
        lstnlmx_export[istr:iend] = lstnlmx_left[:]
    istr = int( param_left.NATOM ) if left else 0
    iend = istr + int( param_center.NATOM )
    lstnlmx_export[istr:iend] = lstnlmx_center[:]
    if right:
        istr = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 )
        iend = istr + int( param_right.NATOM )
        lstnlmx_export[istr:iend] = lstnlmx_right[:]
    if debug:
        print( "%s lstnlmx_export = %s" % ( PROMPT, lstnlmx_export ) )
    try:
        export_data( lstnlmx_export, dir_export, FILE_LSTNLMX, FILE_LSTNLMX.replace( ".info", ".data" ), FILE_LSTNLMX.split( "." )[0], "unformatted" )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import PAW pseudopotential parameter dij
    #--
    print( "%s Import PAW pseudopotential parameter dij." % PROMPT )
    try:
        dij_center = import_data( os.path.join( dir_center, FILE_DIJ ), "dij" )
        dij_left   = import_data( os.path.join( dir_left,   FILE_DIJ ), "dij" ) if left  else None
        dij_right  = import_data( os.path.join( dir_right,  FILE_DIJ ), "dij" ) if right else None
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s dij_left.shape   = %s" % ( PROMPT, dij_left.shape )   )
        print( "%s dij_center.shape = %s" % ( PROMPT, dij_center.shape ) )
        print( "%s dij_right.shape  = %s" % ( PROMPT, dij_right.shape )  )


    #--
    #-- Export PAW pseudopotential parameter dij
    #--
    print( "%s Export PAW pseudopotential parameter dij." % PROMPT )
    dij_export = numpy.zeros( ( int( param_export.NPRJMX ), int( param_export.NPRJMX ), int( param_export.NUMS ), int( param_export.NATOM ) ), dtype=numpy.float64, order="F" )
    if left:
        istr = 0
        iend = int( param_left.NATOM )
        dij_export[:,:,:,istr:iend] = dij_left[:,:,:,:]
    istr = int( param_left.NATOM ) if left else 0
    iend = istr + int( param_center.NATOM )
    dij_export[:,:,:,istr:iend] = dij_center[:,:,:,:]
    if right:
        istr = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 )
        iend = istr + int( param_right.NATOM )
        dij_export[:,:,:,istr:iend] = dij_right[:,:,:,:]
    if debug:
        print( "%s dij_export.shape = %s" % ( PROMPT, dij_export.shape ) )
    try:
        export_data( dij_export, dir_export, FILE_DIJ, FILE_DIJ.replace( ".info", ".data" ), FILE_DIJ.split( "." )[0], "unformatted" )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import PAW pseudopotential parameter sij
    #--
    print( "%s Import PAW pseudopotential parameter sij." % PROMPT )
    try:
        sij_center = import_data( os.path.join( dir_center, FILE_SIJ ), "sij" )
        sij_left   = import_data( os.path.join( dir_left,   FILE_SIJ ), "sij" ) if left  else None
        sij_right  = import_data( os.path.join( dir_right,  FILE_SIJ ), "sij" ) if right else None
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s sij_left.shape   = %s" % ( PROMPT, sij_left.shape )   )
        print( "%s sij_center.shape = %s" % ( PROMPT, sij_center.shape ) )
        print( "%s sij_right.shape  = %s" % ( PROMPT, sij_right.shape )  )


    #--
    #-- Export PAW pseudopotential parameter sij
    #--
    print( "%s Export PAW pseudopotential parameter sij." % PROMPT )
    sij_export = numpy.zeros( ( int( param_export.NPRJMX ), int( param_export.NPRJMX ), int( param_export.NATOM ) ), dtype=numpy.float64, order="F" )
    if left:
        istr = 0
        iend = int( param_left.NATOM )
        sij_export[:,:,istr:iend] = sij_left[:,:,:]
    istr = int( param_left.NATOM ) if left else 0
    iend = istr + int( param_center.NATOM )
    sij_export[:,:,istr:iend] = sij_center[:,:,:]
    if right:
        istr = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 )
        iend = istr + int( param_right.NATOM )
        sij_export[:,:,istr:iend] = sij_right[:,:,:]
    if debug:
        print( "%s sij_export.shape = %s" % ( PROMPT, sij_export.shape ) )
    try:
        export_data( sij_export, dir_export, FILE_SIJ, FILE_SIJ.replace( ".info", ".data" ), FILE_SIJ.split( "." )[0], "unformatted" )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import PAW pseudopotential list vectors lstnl
    #--
    print( "%s Import PAW pseudopotential list vectors lstnl." % PROMPT )
    try:
        lstnl_center = import_data( os.path.join( dir_center, FILE_LSTNL ), "lstnl" )
        lstnl_left   = import_data( os.path.join( dir_left,   FILE_LSTNL ), "lstnl" ) if left  else None
        lstnl_right  = import_data( os.path.join( dir_right,  FILE_LSTNL ), "lstnl" ) if right else None
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s lstnl_left.shape   = %s" % ( PROMPT, lstnl_left.shape )   )
        print( "%s lstnl_center.shape = %s" % ( PROMPT, lstnl_center.shape ) )
        print( "%s lstnl_right.shape  = %s" % ( PROMPT, lstnl_right.shape )  )


    #--
    #-- Reconstruct PAW pseudopotential list vectors lstnl
    #--
    print( "%s Reconstruct PAW pseudopotential list vectors lstnl." % ( PROMPT, ) )
    izshift_left   = 0
    izshift_center = 2 * int( param_left.NZMAX  )  if left  else 0
    izshift_right  = 2 * int( param_center.NZMAX ) + ( 2 * int( param_left.NZMAX  )  if left  else 0 )
    if debug:
        print( "%s param_left.NZMAX   = %4d, izshift_left   = %4d" % ( PROMPT, int( param_left.NZMAX ),   izshift_left )   )
        print( "%s param_center.NZMAX = %4d, izshift_center = %4d" % ( PROMPT, int( param_center.NZMAX ), izshift_center ) )
        print( "%s param_right.NZMAX  = %4d, izshift_right  = %4d" % ( PROMPT, int( param_right.NZMAX ),  izshift_right )  )
    npmax_export = 8 * int( param_export.NPXMAX ) * int( param_export.NPYMAX ) * int( param_export.NPZMAX )
    lstnl_export = numpy.zeros( ( npmax_export, int( param_export.NATOM ) ), dtype=numpy.int32, order="F" )
    nxy = 4 * int( param_export.NXMAX ) * int( param_export.NYMAX )
    if left:
        iaofs = 0
        for ia, atom in enumerate( atoms_left ):
#            if debug: print( "%s ia + iaofs = %3d" % ( PROMPT, ia + iaofs ) )
            for i in xrange( lstnlmx_left[ia] ):
                ( ixy, iz ) = ( ( lstnl_left[i,ia] - 1 ) % nxy + 1, ( lstnl_left[i,ia] - 1 ) // nxy + 1 )         #-- XY & Z indeces in the ORIGINAL cell
                zz = ( iz - 0.5 ) * float( param_left.ZMAX ) / int( param_left.NZMAX ) - float( param_left.ZMAX ) #-- Z coordinate in the ORIGINAL cell
                iz -= int( math.floor( ( zz - atom.z ) / ( 2 * float( param_left.ZMAX ) ) + 0.5 ) ) * ( 2 * int( param_left.NZMAX ) ) #-- Unfold z-index
                jxy = ixy
                jz  = ( izshift_left + iz - 1 ) % ( 2 * int( param_export.NZMAX ) ) + 1
                lstnl_export[i,iaofs+ia] = ( jz - 1 ) * nxy + jxy
    iaofs = int( param_left.NATOM ) if left else 0
    for ia, atom in enumerate( atoms_center ):
#        if debug: print( "%s ia + iaofs = %3d" % ( PROMPT, ia + iaofs ) )
        for i in xrange( lstnlmx_center[ia] ):
            ( ixy, iz ) = ( ( lstnl_center[i,ia] - 1 ) % nxy + 1, ( lstnl_center[i,ia] - 1 ) // nxy + 1 )           #-- XY & Z indeces in the ORIGINAL cell
            zz = ( iz - 0.5 ) * float( param_center.ZMAX ) / int( param_center.NZMAX ) - float( param_center.ZMAX ) #-- Z coordinate in the ORIGINAL cell
            iz -= int( math.floor( ( zz - atom.z ) / ( 2 * float( param_center.ZMAX ) ) + 0.5 ) ) * ( 2 * int( param_center.NZMAX ) ) #-- Unfold z-index
            jxy = ixy
            jz  = ( izshift_center + iz - 1 ) % ( 2 * int( param_export.NZMAX ) ) + 1
            lstnl_export[i,iaofs+ia] = ( jz - 1 ) * nxy + jxy
    if right:
        iaofs = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 )
        for ia, atom in enumerate( atoms_right ):
#            if debug: print( "%s ia + iaofs = %3d" % ( PROMPT, ia + iaofs ) )
            for i in xrange( lstnlmx_right[ia] ):
                ( ixy, iz ) = ( ( lstnl_right[i,ia] - 1 ) % nxy + 1, ( lstnl_right[i,ia] - 1 ) // nxy + 1 )         #-- XY & Z indeces in the ORIGINAL cell
                zz = ( iz - 0.5 ) * float( param_right.ZMAX ) / int( param_right.NZMAX ) - float( param_right.ZMAX ) #-- Z coordinate in the ORIGINAL cell
                iz -= int( math.floor( ( zz - atom.z ) / ( 2 * float( param_right.ZMAX ) ) + 0.5 ) ) * ( 2 * int( param_right.NZMAX ) ) #-- Unfold z-index
                jxy = ixy
                jz  = ( izshift_right + iz - 1 ) % ( 2 * int( param_export.NZMAX ) ) + 1
                lstnl_export[i,iaofs+ia] = ( jz - 1 ) * nxy + jxy


    #--
    #-- Check PAW pseudopotential list vectors lstnl
    #--
    print( "%s Check PAW pseudopotential list vectors lstnl." % PROMPT )
    print( "%s param_export.NZMAX = %d" % ( PROMPT, int( param_export.NZMAX ) ) )
    nxy  = 4 * int( param_export.NXMAX ) * int( param_export.NYMAX )
    nxyz = 8 * int( param_export.NXMAX ) * int( param_export.NYMAX ) * int( param_export.NZMAX )
    for ia, atom in enumerate( atoms_export ):
        ( izmin, izmax ) = ( 2 * int( param_export.NZMAX ), 1 )
        icnt = 0
        for i in xrange( lstnlmx_export[ia] ):
            if lstnl_export[i,ia] < 1 or nxyz < lstnl_export[i,ia]:
                raise Exception( "%s ERROR: lstnl_export[i,ia] < 1 or nxyz < lstnl_export[i,ia]. i = %d, ia = %d" % ( PROMPT, i, ia ) )
            ( ixy, iz )  = ( ( lstnl_export[i,ia] - 1 ) %  nxy + 1 , ( lstnl_export[i,ia] - 1 ) // nxy + 1 )        #-- XY & Z indeces in the EXTRACTED cell
            zz = ( iz - 0.5 ) * float( param_export.ZMAX ) / int( param_export.NZMAX ) - float( param_export.ZMAX ) #-- Z coordinate in the EXTRACTED cell
            iz -= int( math.floor( ( zz - atom.z ) / ( 2 * float( param_export.ZMAX ) ) + 0.5 ) ) * ( 2 * int( param_export.NZMAX ) ) #-- Unfold z-index
            ( izmin, izmax ) = ( min( izmin, iz ), max( izmax, iz ) )
            izext = izmax - izmin + 1
            icnt += 1 if int( math.floor( ( zz - atom.z ) / ( 2 * float( param_export.ZMAX ) ) + 0.5 ) ) == 0 else 0
        print( "%s ia = %4d, z = %12.6f, izmin = %4d, izmax = %4d, range = %4d, icnt = %4d, lstnlmx = %6d" % ( PROMPT, ia + 1, atom.z, izmin, izmax, izext, icnt, lstnlmx_export[ia] ) )
        if( izmax - izmin + 1 > 2 * int( param_export.NZMAX ) ):
            raise Exception( "%s ERROR: izmax - izmin + 1 > 2 * int( param_export.NZMAX )." % ( PROMPT, ) )
#        if debug:
#            hist = [ 0 for i in xrange( izmax - izmin + 1 ) ]
#            for i in xrange( lstnlmx_export[ia] ):
#                ( ixy, iz )  = ( ( lstnl_export[i,ia] - 1 ) %  nxy + 1 , ( lstnl_export[i,ia] - 1 ) // nxy + 1 )        #-- XY & Z indeces in the EXTRACTED cell
#                zz = ( iz - 0.5 ) * float( param_export.ZMAX ) / int( param_export.NZMAX ) - float( param_export.ZMAX ) #-- Z coordinate in the EXTRACTED cell
#                iz -= int( math.floor( ( zz - atom.z ) / ( 2 * float( param_export.ZMAX ) ) + 0.5 ) ) * ( 2 * int( param_export.NZMAX ) ) #-- Unfold z-index
#                hist[iz-izmin] += 1
#            print( "%s hist = %s" % ( PROMPT, hist ) )


    #--
    #-- Export PAW pseudopotential list vectors lstnl
    #--
    print( "%s Export PAW pseudopotential list vectors lstnl." % PROMPT )
    try:
        export_data( lstnl_export, dir_export, FILE_LSTNL, FILE_LSTNL.replace( ".info", ".data" ), FILE_LSTNL.split( "." )[0], "unformatted" )
    except Exception as err:
        raise Exception( err )


    #--
    #-- Import PAW pseudopotential prjectors vnlcp
    #--
    print( "%s Import PAW pseudopotential prjectors vnlcp." % PROMPT )
    try:
        vnlcp_center = import_data( os.path.join( dir_center, FILE_VNLCP ), "vnlcp" )
        vnlcp_left   = import_data( os.path.join( dir_left,   FILE_VNLCP ), "vnlcp" ) if left  else None
        vnlcp_right  = import_data( os.path.join( dir_right,  FILE_VNLCP ), "vnlcp" ) if right else None
    except Exception as err:
        raise Exception( err )
    if debug:
        print( "%s vnlcp_left.shape   = %s" % ( PROMPT, vnlcp_left.shape )   )
        print( "%s vnlcp_center.shape = %s" % ( PROMPT, vnlcp_center.shape ) )
        print( "%s vnlcp_right.shape  = %s" % ( PROMPT, vnlcp_right.shape )  )


    #--
    #-- Reconstruct PAW pseudopotential prjectors vnlcp
    #--
    print( "%s Reconstruct PAW pseudopotential prjectors vnlcp." % PROMPT )
    npmax_export = 8 * int( param_export.NPXMAX ) * int( param_export.NPYMAX ) * int( param_export.NPZMAX )
    vnlcp_export = numpy.zeros( ( npmax_export, int( param_export.NPRJMX ), int( param_export.NATOM ) ), dtype=numpy.float64, order="F" )
    if debug:
        print( "%s vnlcp_export.shape = %s" % ( PROMPT, vnlcp_export.shape ) )
    npmax_center = 8 * int( param_center.NPXMAX ) * int( param_center.NPYMAX ) * int( param_center.NPZMAX )
    npmax_left   = 8 * int( param_left.NPXMAX )   * int( param_left.NPYMAX )   * int( param_left.NPZMAX )   if left  else 0
    npmax_right  = 8 * int( param_right.NPXMAX )  * int( param_right.NPYMAX )  * int( param_right.NPZMAX )  if right else 0
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    if left:
        istr = 0
        iend = int( param_left.NATOM )
        if debug: print( "%s istr = %3d, iend = %3d, npmax_left   = %3d" % ( PROMPT, istr, iend, npmax_left ) )
        vnlcp_export[0:npmax_left,:,istr:iend] = vnlcp_left[:,:,:]
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    istr = int( param_left.NATOM ) if left else 0
    iend = istr + int( param_center.NATOM )
    if debug: print( "%s istr = %3d, iend = %3d, npmax_center = %3d" % ( PROMPT, istr, iend, npmax_center ) )
    vnlcp_export[0:npmax_center,:,istr:iend] = vnlcp_center[:,:,:]
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    if right:
        istr = int( param_center.NATOM ) + ( int( param_left.NATOM ) if left else 0 )
        iend = istr + int( param_right.NATOM )
        if debug: print( "%s istr = %3d, iend = %3d, npmax_right  = %3d" % ( PROMPT, istr, iend, npmax_right ) )
        vnlcp_export[0:npmax_right,:,istr:iend] = vnlcp_right[:,:,:]
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


    #--
    #-- Check PAW pseudopotential prjectors vnlcp
    #--
    print( "%s Check PAW pseudopotential prjectors vnlcp." % PROMPT )
    work0 = numpy.zeros( ( 2 * int( param_export.NXMAX ), 2 * int( param_export.NYMAX ), 2 * int( param_export.NZMAX ), int( param_export.NPRJMX ) ), dtype=numpy.float64, order="F" )
    work1 = numpy.zeros( ( 2 * int( param_export.NXMAX ), 2 * int( param_export.NYMAX ), 2 * int( param_export.NZMAX ), int( param_export.NPRJMX ) ), dtype=numpy.float64, order="F" )
    nx   = 2 * int( param_export.NXMAX )
    nxy  = 4 * int( param_export.NXMAX ) * int( param_export.NYMAX )
    nxyz = 8 * int( param_export.NXMAX ) * int( param_export.NYMAX ) * int( param_export.NZMAX )
    for ia, atom in enumerate( atoms_export ):
        work0[:,:,:,:] = 0.0
        work1[:,:,:,:] = 0.0
        for i in xrange( int( lstnlmx_export[ia] ) ):
            iz = ( lstnl_export[i,ia] - 1 ) // nxy + 1
            iy = ( ( lstnl_export[i,ia] - 1 ) %  nxy ) // nx + 1
            ix = ( lstnl_export[i,ia] - 1 ) %  nx + 1
            work0[ix-1,iy-1,iz-1,:] = vnlcp_export[i,:,ia]
            zz = ( iz - 0.5 ) * float( param_export.ZMAX ) / int( param_export.NZMAX ) - float( param_export.ZMAX ) #-- Z coordinate in the EXTRACTED cell
            iz -= int( math.floor( ( zz - atom.z ) / ( 2 * float( param_export.ZMAX ) ) + 0.5 ) ) * ( 2 * int( param_export.NZMAX ) ) #-- Unfold z-index
            if 1 <= iz <= 2 * int( param_export.NZMAX ):
                work1[ix-1,iy-1,iz-1,:] = vnlcp_export[i,:,ia]
        print( "%s ia = %4d, ngrid = %6d / %6d" % ( PROMPT, ia + 1, numpy.count_nonzero( numpy.sum( work1, axis=3 ) ), numpy.count_nonzero( numpy.sum( work0, axis=3 ) ) ) )


    #--
    #-- Export PAW pseudopotential prjectors vnlcp
    #--
    print( "%s Export PAW pseudopotential prjectors vnlcp." % PROMPT )
    try:
        export_data( vnlcp_export, dir_export, FILE_VNLCP, FILE_VNLCP.replace( ".info", ".data" ), FILE_VNLCP.split( "." )[0], "unformatted" )
    except Exception as err:
        raise Exception( err )


    return 0


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
if __name__ == "__main__":

    import sys

    try:
        ierr = combine_potentialdata( dir_center=sys.argv[1], dir_left=sys.argv[2], dir_right=sys.argv[3], dir_export="./", debug=True )
    except Exception as errmsg:
        sys.exit( errmsg )

    sys.exit( 0 )
