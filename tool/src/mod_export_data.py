#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#-- 
#-- export_data
#--
def export_data( data, dir, filename_info, filename_data, name, form="unformatted" ):


    #--
    #-- Import module
    #--
    import sys, numpy
    from mod_export_info               import export_info
    from mod_write_fortran_unformatted import write_fortran_unformatted


    #--
    #-- Parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Display dummy argument
    #--
    print( "%s dir           = %s" % ( PROMPT, dir )           )
    print( "%s filename_info = %s" % ( PROMPT, filename_info ) )
    print( "%s filename_data = %s" % ( PROMPT, filename_data ) )
    print( "%s name          = %s" % ( PROMPT, name )          )
    print( "%s form          = %s" % ( PROMPT, form )          )
    if not form in ( "unformatted" ):
        raise Exception( "%s ERROR: not form in ( unformatted )" % ( PROMPT ) )


    #--
    #-- Export info file
    #--
    filename = dir + filename_info
    section  = name
    if data.dtype == "int32":
        variabletype = "integer"
    elif data.dtype == "float64":
        variabletype = "real(8)"
    elif data.dtype == "complex128":
        variabletype = "complex(8)"
    else:
        raise Exception( "%s ERROR: not data.dtype in ( int32, float64, complex128 )" % ( PROMPT ) )
    variabledim    = data.ndim
    variableshape  = str( data.shape )[1:-1].replace( ",", "" )
    datafilename   = filename_data
    datafiletype   = form
    databyteorder  = sys.byteorder + "_endian"
    dataoffsetbyte = 0
    recordmarkbyte = 4 if form == "unformatted" else 0
    databyte       = data.nbytes
    try:
        export_info( filename, section, variabletype, variabledim, variableshape, datafilename, datafiletype, databyteorder, dataoffsetbyte, recordmarkbyte, databyte )
    except Exception as err:
        raise Exception( "%s %s" % ( PROMPT, err ) )


    #--
    #-- Export data file
    #--
    if form == "unformatted":
        filename = dir + filename_data
        write_fortran_unformatted( filename, data, write_type=None, reclength=4, byteswap=False )
    else:
        raise Exception( "%s ERROR: form != unformatted" % ( PROMPT ) )


    return True
#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
