#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#-- Define element name
#--
ELEMENTNAME = [ "H",  "He", "Li", "Be", "B",  "C",  "N",  "O",  "F",  "Ne",
                "Na", "Mg", "Al", "Si", "P",  "S",  "Cl", "Ar", "K",  "Ca",
                "Sc", "Ti", "V",  "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
                "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y",  "Zr",
                "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn",
                "Sb", "Te", "I",  "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd",
                "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb",
                "Lu", "Hf", "Ta", "W",  "Re", "Os", "Ir", "Pt", "Au", "Hg" ]
#--
#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0




#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#-- class atom
#--
class atom:


    #----1---------2---------3---------4---------5---------6---------7---------8---------9---------0
    #--
    def __init__( self, x, y ,z ,nz ):


        try:
            self.x    = float( x )
            self.y    = float( y )
            self.z    = float( z )
            self.nz   = int( nz )
            self.name = ELEMENTNAME[ int( nz ) - 1 ]
        except Exception:
            raise ValueError, "ERROR: constructing atom is failed."


        return
    #--
    #----1---------2---------3---------4---------5---------6---------7---------8---------9---------0


    #----1---------2---------3---------4---------5---------6---------7---------8---------9---------0
    #--
    #-- Stringify
    #--
    def __str__( self ):


        return "%2d %s %12.8f %12.8f %12.8f" % ( self.nz, self.name, self.x, self.y, self.z )
    #--
    #----1---------2---------3---------4---------5---------6---------7---------8---------9---------0


#--
#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0


#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#-- import_atomxyz
#--
def import_atomxyz( filename, natom ):


    #--
    #-- Set parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Import module
    #--
    import re, sys


    #--
    #-- Display dummy argument
    #--
    print( "%s filename = %s" % ( PROMPT, filename ) )
    print( "%s natom    = %d" % ( PROMPT, natom )    )


    #--
    #-- Define comment out
    #--
    re_comment = re.compile( "^[#!]", re.IGNORECASE )
    re_number  = re.compile( "([0-9])[ed]([+-]?[0-9])", re.IGNORECASE )


    #--
    #-- Read the atomic coordinate file
    #--
    with open( filename, "r" ) as fp:
        lines = [ line.strip() for line in fp.readlines() ]


    #--
    #-- Remove comment line
    #--
    atomlines = []
    for line in lines:
        if re_comment.match( line ) or line == "": continue
        atomlines.append( line )
    del lines


    #--
    #-- Extract atomic coordinate
    #--
    atoms = []
    for i, atomline in enumerate( atomlines ):
        if i == natom:
            break
        items = re_number.sub( r"\1e\2", atomline ).split()
        if len( items ) < 4:
            raise ValueError, "len( items ) < 4"
        try:
            work = atom( float( items[0] ), float( items[1] ), float( items[2] ), int( items[3] ) )
        except Exception, errmsg:
            sys.exit( errmsg )
        atoms.append( work )


    #--
    #-- Display atomic coordinate
    #--
    for i in xrange( len( atoms ) ):
        print( "%s i = %4d nz = %4d %s x = %12.8f y = %12.8f z = %12.8f" % (PROMPT, i + 1, atoms[i].nz, atoms[i].name, atoms[i].x, atoms[i].y, atoms[i].z ) )


    return atoms
#--
#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0


