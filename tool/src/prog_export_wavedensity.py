#!/bin/env python
# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- prog_export_wavedensity.py
#--
if __name__ == "__main__":
    """ Python script for export charge density distribution for 3D visualization

    """


    #--
    #-- Import module
    #--
    import os.path
    import argparse
    import sys
    from mod_export_wavedensity import export_wavedensity


    #--
    #-- Set parameter
    #--
    PROMPT = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"


    #--
    #-- Parse command option and argument
    #--
    parser = argparse.ArgumentParser( description="Transform wave functions into charge density and export it to file." )

    parser.add_argument( "--debug", \
                         dest    = "debug", \
                         action  = "store_true", \
                         default = False, \
                         help    = "flag for the debug mode. (default: False)" )

    parser.add_argument( "--file-info", \
                         dest     = "fileinfo", \
                         action   = "store", \
                         type     = str, \
                         required = True, \
                         help     = "path to the information file. required option" )

    parser.add_argument( "--file-param", \
                         dest     = "fileparam", \
                         action   = "store", \
                         type     = str, \
                         required = True, \
                         help     = "path to the param file. required option" )

    parser.add_argument( "--along-z", \
                         dest    = "along_z", \
                         action  = "store_true", \
                         default = False, \
                         help    = "wave density is integrated over each xy plane, and output the integrated density as a function of the z axis. (default: False)" )

    #parser.add_argument( "--logscale", \
    #                     dest    = "logscale", \
    #                     action  = "store_true", \
    #                     default = False, \
    #                     help    = "wave density is output in log scale. This is valid only for exporting xsf file. (default: False)" )

    parser.add_argument( "--periodic", \
                         dest    = "periodic", \
                         action  = "store_true", \
                         default = False, \
                         help    = "wave density is output in periodic xsf file. This is valid only for exporting xsf file. (default: False)" )

    parser.add_argument( "--wavefunction", \
                         dest    = "wavefunction", \
                         action  = "store_true", \
                         default = False, \
                         help    = "wave function is output in two xsf files for real and imaginary parts. This is valid only for exporting xsf file. (default: False)" )

    print parser.parse_args()
    args = parser.parse_args()


    #--
    #-- Export wavedensity
    #--
    fileinfo     = args.fileinfo
    fileparam    = args.fileparam
    along_z      = args.along_z
    #logscale     = args.logscale
    logscale     = False
    periodic     = args.periodic
    wavefunction = args.wavefunction
    debug        = args.debug
    try:
        ierr = export_wavedensity( fileinfo, fileparam, along_z=along_z, logscale=logscale, periodic=periodic, wavefunction=wavefunction, debug=debug )
    except Exception, errmsg:
        sys.exit( errmsg )


    sys.exit( ierr )

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
