#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#-- 
#-- export_info
#--
def export_info( filename, section, variabletype, variabledim, variableshape, datafilename, datafiletype, databyteorder, dataoffsetbyte, recordmarkbyte, databyte ):


    #--
    #-- Import module
    #--
    import ConfigParser


    #--
    #-- Parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Display dummy argument
    #--
    print "%s filename       = %s" % ( PROMPT, filename       )
    print "%s section        = %s" % ( PROMPT, section        )
    print "%s variabletype   = %s" % ( PROMPT, variabletype   )
    print "%s variabledim    = %d" % ( PROMPT, variabledim    )
    print "%s variableshape  = %s" % ( PROMPT, variableshape  )
    print "%s datafilename   = %s" % ( PROMPT, datafilename   )
    print "%s datafiletype   = %s" % ( PROMPT, datafiletype   )
    print "%s databyteorder  = %s" % ( PROMPT, databyteorder  )
    print "%s dataoffsetbyte = %d" % ( PROMPT, dataoffsetbyte )
    print "%s recordmarkbyte = %d" % ( PROMPT, recordmarkbyte )
    print "%s databyte       = %d" % ( PROMPT, databyte       )


    #--
    #--
    #--
    conf = ConfigParser.SafeConfigParser() #-- Construct 


    #--
    #-- Add section
    #--
    conf.add_section( section )


    #--
    #-- Set option-value pairs
    #-- 
    conf.set( section, "variabletype",   str( variabletype )   )
    conf.set( section, "variabledim",    str( variabledim )    )
    conf.set( section, "variableshape",  str( variableshape )  )
    conf.set( section, "datafilename",   str( datafilename )   )
    conf.set( section, "datafiletype",   str( datafiletype )   )
    conf.set( section, "databyteorder",  str( databyteorder )  )
    conf.set( section, "dataoffsetbyte", str( dataoffsetbyte ) )
    conf.set( section, "recordmarkbyte", str( recordmarkbyte ) )
    conf.set( section, "databyte"      , str( databyte )       )


    #--
    #-- Export config
    #--
    with open( filename, "w" ) as fp:
        conf.write( fp )


    return True
#-- 
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
