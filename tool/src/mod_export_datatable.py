# -*- coding: utf-8 -*-                                                                                                                               
#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0                                                  
#--                                                                                                                                                   
#-- export_datatable                                                                                                                                   
#--                                                                                                                                                   
def export_datatable( filename, table, labels=(), debug=False ):                                                                                       


    #--
    #-- Import
    #--
    import os


    #--
    #-- Parameter
    #--          
    PROMPT = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"


    #--
    #-- Display argument
    #--                 
    if debug:
        print( "%s filename    = %s" % ( PROMPT, filename )    )
        print( "%s table.shape = %s" % ( PROMPT, table.shape ) )
        print( "%s labels      = %s" % ( PROMPT, labels )      )
        print( "%s debug       = %s" % ( PROMPT, debug )       )


    #--
    #-- Check table dimension
    #--                      
    if table.ndim != 2:      
        raise Exception( "%s ERROR: table.ndim != 2. table.ndim = %d" % ( PROMPT, table.ndim ) )


    #--
    #-- Check labels
    #--             
    if len(labels) > table.shape[1]:
        raise Exception( "%s ERROR: len(labels) > table.shape[1]. len(labels) = %d, table.shape[1] = %d" % ( PROMPT, len(labels), table.shape[1] ) )


    #--
    #-- Create header
    #--
    buf = ""
    if len(labels) > 0:
        for label in labels:
            buf += " %s" % label
        buf += "\n"


    #--
    #-- Create body
    #--
    for i in xrange( table.shape[0] ):
        for j in xrange( table.shape[1] ):
            if isinstance( table[i,j], int ):
                buf += " %8d"    % ( table[i,j] )
            else:
                buf += " %16.8e" % ( table[i,j] )
        buf += "\n"


    #--
    #-- Write data to file
    #--
    if debug:
        for i, line in enumerate( buf.split("\n") ):
            print( "%s %4.4d: %s" % ( PROMPT, i + 1, line ) )
    else:
        with open( filename, "w" ) as fp:
            fp.write( buf )


    return 0


#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#--
if __name__ == "__main__":

    import sys
    import numpy

    table = numpy.array( [ i for i in xrange(25) ], dtype=numpy.float64, order="F" ).reshape( ( 5, 5 ) )

    try:
        write_datatable( "dammy.dat", table, labels=( "a", "b", "c", "d", "e", "z" ), debug=True )
    except Exception, errmsg:
        sys.exit( errmsg )

    sys.exit(0)

