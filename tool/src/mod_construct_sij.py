# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- construct_sij
#--
def construct_sij( dir_exe, debug=False ):
    """ Python function for constructing the list of PAW sij matrices.

    The python function receives the path to the directory containing the PAW data set files, and 
    automatically seek the necessary files to construct the list of sij numpy arrays.

    Args:
        dir_exe: The path of the directory containing necessary PAW projector data files.
        debug: Flag for debug mode

    Returns:
        The PAW projector data is returned as the array, which contains numpy.arrays of PAW 
        projector data over a real-space grids for each atom.
    """


    #--
    #-- Import module
    #--
    import os.path
    import numpy

    import mod_param_f
    import mod_import_data


    #--
    #-- Set parameter
    #--
    PROMPT     = __name__.upper() + " >>>"
    DIR_EXE    = dir_exe if dir_exe[-1] == "/" else dir_exe + "/"
    FILE_PARAM = DIR_EXE + "param.f"
    FILE_NPRJ  = DIR_EXE + "nprj.info"
    FILE_SIJ   = DIR_EXE + "sij.info"


    #--
    #-- Display dummy argument
    #--
    if debug:
        print( "%s dir_exe = %s" % ( PROMPT, dir_exe ) )
        print( "%s debug   = %s" % ( PROMPT, debug )   )


    #--
    #-- Check dir and files
    #--
    if debug:
        print( "%s DIR_EXE    = %s" % ( PROMPT, DIR_EXE )    )
        print( "%s FILE_PARAM = %s" % ( PROMPT, FILE_PARAM ) )
        print( "%s FILE_NPRJ    = %s" % ( PROMPT, FILE_NPRJ )    )
        print( "%s FILE_SIJ   = %s" % ( PROMPT, FILE_SIJ )   )
    if not os.path.isdir( DIR_EXE ):
        raise Exception( "%s ERROR: not os.path.isdir( DIR_EXE ). DIR_EXE = %s" % ( PROMPT, DIR_EXE ) )
    if not os.path.isfile( FILE_PARAM ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_PARAM ). FILE_PARAM = %s" % ( PROMPT, FILE_PARAM ) )
    if not os.path.isfile( FILE_NPRJ ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_NPRJ ). FILE_NPRJ = %s" % ( PROMPT, FILE_NPRJ ) )
    if not os.path.isfile( FILE_SIJ ):
        raise Exception( "%s ERROR: not os.path.isfile( FILE_SIJ ). FILE_SIJ = %s" % ( PROMPT, FILE_SIJ ) )


    #--
    #-- Import FILE_PARAM
    #--
    print( "%s Import FILE_PARAM" % ( PROMPT ) )
    param = mod_param_f.param_f( FILE_PARAM )
    if debug:
        print( "%s param.__dict__ = %s" % ( PROMPT, param.__dict__ ) )


    #--
    #-- Set number of atom to be processed
    #--
#    XMAX   = float( param.XMAX )
#    YMAX   = float( param.YMAX )
#    ZMAX   = float( param.ZMAX )
    NXMAX  = int( param.NXMAX )
    NYMAX  = int( param.NYMAX )
    NZMAX  = int( param.NZMAX )
    NPXMAX = int( param.NPXMAX )
    NPYMAX = int( param.NPYMAX )
    NPZMAX = int( param.NPZMAX )
    NATOM  = int( param.NATOM )
    NUMS   = int( param.NUMS )
    NPRJMX = int( param.NPRJMX )
#    NF     = int( param.NF )
#    FERM   = float( param.FERM )
    if debug:
#        print( "%s XMAX    = %f" % ( PROMPT, XMAX    ) )
#        print( "%s YMAX    = %f" % ( PROMPT, YMAX    ) )
#        print( "%s ZMAX    = %f" % ( PROMPT, ZMAX    ) )
        print( "%s NXMAX   = %d" % ( PROMPT, NXMAX   ) )
        print( "%s NYMAX   = %d" % ( PROMPT, NYMAX   ) )
        print( "%s NZMAX   = %d" % ( PROMPT, NZMAX   ) )
        print( "%s NPXMAX  = %d" % ( PROMPT, NPXMAX  ) )
        print( "%s NPYMAX  = %d" % ( PROMPT, NPYMAX  ) )
        print( "%s NPZMAX  = %d" % ( PROMPT, NPZMAX  ) )
        print( "%s NATOM   = %d" % ( PROMPT, NATOM   ) )
#        print( "%s NUMS    = %d" % ( PROMPT, NUMS    ) )
        print( "%s NPRJMX  = %d" % ( PROMPT, NPRJMX  ) )
#        print( "%s NF      = %d" % ( PROMPT, NF      ) )
#        print( "%s FERM    = %f" % ( PROMPT, FERM    ) )
               
               
    #--        
    #-- Import FILE_NPRJ
    #--        
    print( "%s Import FILE_NPRJ" % ( PROMPT ) )
    nprj = mod_import_data.import_data( FILE_NPRJ, "nprj", debug )
    if debug:  
        print( "%s nprj.shape = %s" % ( PROMPT, nprj.shape ) )
        print( "%s nprj       = %s" % ( PROMPT, nprj )       )
    if nprj.shape[0] != NATOM:
        raise Exception( "%s ERROR: nprj.shape[0] != NATOM. nprj.shape[0] = %d NATOM = %d" % ( PROMPT, nprj.shape[0], NATOM ) )
               
               
    #--        
    #-- Import FILE_SIJ
    #--        
    print( "%s Import FILE_SIJ" % ( PROMPT ) )
    sij = mod_import_data.import_data( FILE_SIJ, "sij", debug )
    if debug:  
        print( "%s sij.shape = %s" % ( PROMPT, sij.shape ) )
    if sij.shape[0] != NPRJMX:
        raise Exception( "%s ERROR: sij.shape[0] != NPRJMX. sij.shape[0] = %d NPRJMX = %d" % ( PROMPT, sij.shape[0], NPRJMX ) )
    if sij.shape[1] != NPRJMX:
        raise Exception( "%s ERROR: sij.shape[1] != NPRJMX. sij.shape[1] = %d NPRJMX = %d" % ( PROMPT, sij.shape[1], NPRJMX ) )
    if sij.shape[2] != NATOM:
        raise Exception( "%s ERROR: sij.shape[2] != NATOM. sij.shape[2] = %d NATOM = %d" % ( PROMPT, sij.shape[2], NATOM ) )
               
               
    #--
    #-- Construct list of sij numpy matrices
    #--
    print( "%s Construct list of sij numpy matrices" % ( PROMPT ) )
    sijs = []
    for ia in xrange( NATOM ):
        sijs.append( sij[0:nprj[ia],0:nprj[ia],ia] )
    if debug:
        for ia, sij in enumerate( sijs ):
            for i in xrange( sij.shape[0] ):
                str = "%s" % ( PROMPT, )
                for j in xrange( sij.shape[1] ):
                    str += " %8.1e" % ( sij[i,j], )
                print( "%s" % ( str, ) )


    return sijs


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


if __name__ == "__main__":

    import sys
    
    
    try:
        sijs = construct_sij( sys.argv[1], debug=True )
    except Exception, errmsg:
        sys.exit( errmsg )

    print len( sijs )
    for sij in sijs:
        print sij.shape

    sys.exit( 0 )


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
