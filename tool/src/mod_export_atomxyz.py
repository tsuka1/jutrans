#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#-- export_atomxyz
#--
def export_atomxyz( filename, atoms, lattice=None ):


    #--
    #-- Set parameter
    #--
    PROMPT = __name__.upper() + " >>>"


    #--
    #-- Display dummy argument
    #--
    print( "%s filename = %s" % ( PROMPT, filename ) )


    #--
    #--
    #--
    str = "! [x], [y], [z], [atom number], switch [x], [y], [z], [weight], [pp], [na]\n"
    for i, atom in enumerate( atoms ):
        str += " %24.16e %24.16e %24.16e %3d %3d %3d %3d %9.2f %2d %4da\n" % ( atom.x, atom.y, atom.z, atom.nz, 0, 0, 0, 22056.00, 1, i+1 )

    if lattice != None:
        if len( lattice ) < 3:
            raise Exception( "%s ERROR: len( lattice ) < 3. len( lattice ) = %d" % ( PROMPT, len( lattice ) ) )
        str += "\n! Bravais matrix (output only!)\n"
        str += " %24.16e %24.16e %24.16e\n" % ( lattice[0], 0.0, 0.0 )
        str += " %24.16e %24.16e %24.16e\n" % ( 0.0, lattice[1], 0.0 )
        str += " %24.16e %24.16e %24.16e\n" % ( 0.0, 0.0, lattice[2] )

    with open( filename, "w" ) as fp:
        fp.write( str )


    return 0
#--
#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0


