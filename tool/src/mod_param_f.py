# -*- coding: utf-8 -*-
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- Class: param_f
#--
class param_f:


    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    #--
    #-- Class constructor
    #--
    def __init__( self, file_param_f, debug=False ):


        #--
        #-- Import modules
        #--
        import os.path
        import re


        #--
        #-- Set parameters & define string patterns
        #--
        PROMPT           = os.path.splitext(os.path.basename(__file__))[0].upper() + " >>>"
        re_commentline   = re.compile( "^!",                   re.IGNORECASE )
        re_parameterline = re.compile( "parameter",            re.IGNORECASE )
        re_doubleprec    = re.compile( "([0-9])d([+-]?[0-9])", re.IGNORECASE )


        #--
        #-- Display dummy argument
        #--
        if debug:
            print "%s file_param_f = %s" % ( PROMPT, file_param_f )


        #--
        #-- Read the parameter file : file_param_f
        #--
        with open( file_param_f, "r" ) as fp:
            lines = [ line.strip() for line in fp.readlines() ]


        #--
        #-- Extract parameters
        #--
        params = []
        for line in lines:
            if re_commentline.match( line ): #-- Skip comment lines
                continue
            if re_parameterline.search( line ): #-- Extract parameters from the line
                params.append( line.split("::")[1].strip() )


        #--
        # Parse parameters
        #--
        for param in params:
            ( key, val ) = [ item.strip() for item in param.split("=") ] #-- Split string into name & value
            val = re_doubleprec.sub( r"\1e\2", val ) #-- Change char. D/d in a floating number to e
            self.__dict__[ key ] = val
            if debug:
                print "%s %s\t=\t%s" % ( PROMPT, key, self.__dict__[ key ] )


    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    #--
    #-- Stringify
    #--
    def __str__( self ):

        return "%s" % ( self.__dict__, )


#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0




#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- Main routine for test
#--
if __name__ == "__main__":

    import sys

    test = param_f( sys.argv[1], debug=True )
    print test.__dict__

    sys.exit( 0 )

#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
