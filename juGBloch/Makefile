#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#-- Makefile
#--


.SUFFIXES:
.SUFFIXES: .o .d .F90


ROOTDIR := $(HOME)/juTrans


DATADIR := ../juGBloch_data


#OPENMP := no
OPENMP := yes


#PPTYPE := NCPP
PPTYPE := PAW


#BMATRIXTYPE := BMATRIX_DOUBLEPRECISION
BMATRIXTYPE := BMATRIX_DOUBLECOMPLEX

#JUGBLOCHMODE := JUOBM
JUGBLOCHMODE := COMPLEXBAND

#VERBOSE := yes
VERBOSE := no


#-- Preprocessor symbols
PREPROC := 
PREPROC := $(PREPROC) -DDATAPATH=$(strip $(DATADIR))/ 
PREPROC := $(PREPROC) -DPPTYPE=$(strip $(PPTYPE))
PREPROC := $(PREPROC) -DBMATRIXTYPE=$(strip $(BMATRIXTYPE))
PREPROC := $(PREPROC) -DJUGBLOCHMODE=$(strip $(JUGBLOCHMODE))
ifeq ($(strip $(VERBOSE)),yes)
   PREPROC := $(PREPROC) -DVERBOSE
endif


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- Include Makefile.SYSTEM to define machine-dependent configuration
#--
#-- To Makefile.SYSTEM, the variables OPENMP, PREPROC are handed over.
#-- In Makefile.SYSTEM, the variables FC, FFLAGS, LDFLAGS, and PREPROC have to be defined.
#-- FC      Fortran compiler command
#-- FFLAGS  Fortran compiler option
#-- LDFLAGS Fortran linker option
#-- PREPROC Fortran preprocessor option
#--
ifeq ($(shell ls ./ | grep Makefile.SYSTEM ),Makefile.SYSTEM)
include ./Makefile.SYSTEM # including modified Makefile.SYSTEM
else
include ${ROOTDIR}/juGBloch/Makefile.SYSTEM # including default Makefile.SYSTEM
endif


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


#-- Object files
OBJS := mod_mpi.o\
        mod_constant.o\
        mod_parameter.o\
        mod_tool.o\
        mod_lapack.o\
        mod_blacs.o\
        mod_scalapack_interface.o\
        mod_scalapack.o\
        mod_timestamp.o\
        mod_rsdecomp.o\
        mod_ctxt2rscart.o\
        mod_dynamiclist.o\
        mod_iniparser.o\
        mod_vlocal.o\
        mod_localpotential.o\
        mod_atomxyz.o\
        mod_nonlocalfile_ncpp.o\
        mod_nonlocalfile_paw.o\
        mod_elementname.o\
        mod_nonlocal_data.o\
        mod_nonlocalpotential.o\
        mod_bmatrix_lib.o\
        mod_rsfd_coefficient.o\
        mod_singvaldecomp.o\
        mod_bmatrix.o\
        mod_greenfunction_lib.o\
        mod_blacs2rsdecomp.o\
        mod_rsdecomp2blacs.o\
        mod_nonlocal.o\
        mod_cgsolver_kake.o\
        mod_cgsolver_precon.o\
        mod_cgsolver.o\
        mod_greenfunction.o\
        mod_randommatrix.o\
        mod_sakuraisugiura.o\
        mod_gbloch_wave.o\
        mod_gbloch_lib.o\
        mod_gbloch_velocity.o\
        mod_inversematrix.o\
        mod_biorthogonal.o\
        mod_orthocomplement.o\
        mod_selfenergy.o\
        mod_selfenergy_lib.o\
        prog_jugbloch.o


#-- Executive name
EXEC := juGBloch


#-- File search path
VPATH = ./:$(ROOTDIR)/juGBloch/:$(ROOTDIR)/common/:$(ROOTDIR)/juGBloch/src/:$(ROOTDIR)/common/src/


#--
.PHONY: all clean clean_all clean-all


#--
all: $(EXEC)


#-- Link command to generate executive
$(EXEC): $(OBJS)
	$(FC) $(FFLAGS) -o $@ $^ $(LDFLAGS)


#-- Compile command to generate object files
%.o: %.F90
	$(FC) $(FFLAGS) $(PREPROC) -c $<


%.o: %.f
	$(FC) $(FFLAGS) $(PREPROC) -c $<


clean_all clean-all: clean
	rm -f $(EXEC) gmon.out *.dat *.info *.data *.txt *.xsf


clean:
	rm -f *.o *.mod *.optrpt *~


#--
#-- end of Makefile
#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0

