

!#define DEBUG


!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
!--
!-- mod_localpotential
!--
!> @brief  Fortran module for constructing local potential for juGBloch code
!> @author Shigeru Tsukamoto (s.tsukamoto@fz-juelich.de)
!--
module mod_localpotential


  !--
  !-- Module
  !--
  implicit none
  private


  !--
  !-- Subprogram scope
  !--
  public  :: localpotential_construct


  contains


  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
  !--
  !-- localpotential_construct
  !--
  !> @brief   Fortran subroutine for constructing local potential for juGBloch code
  !> @author Shigeru Tsukamoto (s.tsukamoto@fz-juelich.de)
  !> @details The subroutine calls the subroutine import_vlocal() to import local potential data
  !!          from the file specified by parameter variable FILEINFO
  !--
  subroutine localpotential_construct( ctxt, fh, vlocal, ierr )


    !--
    !-- Module
    !--
    use mod_mpi
    use mod_parameter, only : NCPX, NCPY, NCPZ, NSPIN, DATADIR
    use mod_blacs,     only : blacs_gridinfo, blacs_barrier
    use mod_vlocal,    only : vlocal_import
    implicit none


    !--
    !-- Parameter
    !--
    character(len=*), parameter :: PROMPT = "CONSTRUCT_LOCALPOTENTIAL >>>"


    !--
    !-- Dummy argument
    !--
    integer, intent(in)  :: ctxt                          !< BLACS context of processes involved in this routine
    integer, intent(in)  :: fh                            !< MPI stdout file handler
    real(8), intent(out) :: vlocal(NCPX,NCPY,NCPZ,NSPIN)  !< Domain decomposed 4D array for local potential data
    integer, intent(out) :: ierr                          !< Error indicator


    !--
    !-- Local variable
    !--
    integer :: nprow, npcol, myprow, mypcol
    logical :: o


    !----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


    !--
    !-- Check argument & initialize
    !--
    ierr = 0
    !-- Check BLACS context
    call blacs_gridinfo( ctxt, nprow, npcol, myprow, mypcol )
    if( any( [ nprow, npcol, myprow, mypcol ] == -1 ) ) call MPIX_Abort( PROMPT // " ERROR: invalid BLACS context.", ierr )
    !-- Set flag for standard output
    o = ( fh /= MPI_FILE_NULL .and. myprow == 0 .and. mypcol == 0 )


    !--
    !-- Import local part of pseudo + effective potential file
    !--
#ifdef DEBUG
    call blacs_barrier( ctxt, "All" )
#endif
    if( o ) call MPIX_File_write_shared( fh, '(a,x,"Import local part of pseudo + effective potential file")', PROMPT, ierr )
    call vlocal_import( ctxt, fh, trim( DATADIR ), vlocal, ierr )


    !--
    !-- Finalize
    !--
    return


  end subroutine localpotential_construct
  !--
  !-+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


end module mod_localpotential
!--
!---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
