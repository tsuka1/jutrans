#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#-- Makefile
#--


.SUFFIXES:
.SUFFIXES: .o .d .F90

ROOTDIR := $(HOME)/juTrans

DATADIRLFT := ../../Lead/juGBloch_data
DATADIRCTR := ../juOBM_data
DATADIRRGT := ../../Lead/juGBloch_data

#MATCHINGMODE := MATCHINGMODE_FERMILEVEL
MATCHINGMODE := MATCHINGMODE_POTENTIAL 

OPENMP := no
#OPENMP := yes


#PPTYPE := NCPP
PPTYPE := PAW


BMATRIXTYPE := BMATRIX_DOUBLEPRECISION
#BMATRIXTYPE := BMATRIX_DOUBLECOMPLEX


#-- Output (Optional)
#VERBOSE := yes
VERBOSE := no


#-- Import scattering wave functions (Optional)
#IMPORT_SCATTERING_WAVE_FUNCTIONS := yes
IMPORT_SCATTERING_WAVE_FUNCTIONS := no
DIR_SCATTERING_WAVE_FUNCTIONS := ../juTrans_1_ok


#-- Preprocessor symbols
PREPROC :=
PREPROC := $(PREPROC) -DDATAPATHLFT=$(strip $(DATADIRLFT))/
PREPROC := $(PREPROC) -DDATAPATHCTR=$(strip $(DATADIRCTR))/
PREPROC := $(PREPROC) -DDATAPATHRGT=$(strip $(DATADIRRGT))/
PREPROC := $(PREPROC) -DMATCHINGMODE=$(strip $(MATCHINGMODE))
PREPROC := $(PREPROC) -DPPTYPE=$(strip $(PPTYPE))
PREPROC := $(PREPROC) -DBMATRIXTYPE=$(strip $(BMATRIXTYPE))
ifeq ($(strip $(VERBOSE)),yes)
   PREPROC := $(PREPROC) -DVERBOSE
endif
ifeq ($(strip $(AUXILIARYDATA)),yes)
   PREPROC := $(PREPROC) -DIMPORT_GBLOCH_AUXILIARYDATA
endif
ifeq ($(strip $(IMPORT_SCATTERING_WAVE_FUNCTIONS)),yes)
   PREPROC := $(PREPROC) -DIMPORT_SCATTERING_WAVE_FUNCTIONS
   PREPROC := $(PREPROC) -DDIR_SCATTERING_WAVE_FUNCTIONS=$(strip $(DIR_SCATTERING_WAVE_FUNCTIONS))/
endif


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
#--
#-- Include Makefile.SYSTEM to define Machine-dependent configuration
#--
#-- Here, FC, FFLAGS, LDFLAGS, and PREPROC have to be defined
#-- FC      Fortran compiler command
#-- FFLAGS  Fortran compiler option
#-- LDFLAGS Fortran linker option
#-- PREPROC Fortran preprocessor option
#--
ifeq ($(shell ls ./ | grep Makefile.SYSTEM ),Makefile.SYSTEM)
include ./Makefile.SYSTEM
else
include ${ROOTDIR}/juOBM/Makefile.SYSTEM
endif


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


#-- Object file name
OBJS := mod_mpi.o\
        mod_constant.o\
        mod_parameter.o\
        mod_tool.o\
        mod_lapack.o\
        mod_blacs.o\
        mod_scalapack_interface.o\
        mod_scalapack.o\
        mod_timestamp.o\
        mod_rsdecomp.o\
        mod_ctxt2rscart.o\
        mod_dynamiclist.o\
        mod_iniparser.o\
        mod_vlocal.o\
        mod_fuzzycell.o\
        mod_localpotential.o\
        mod_atomxyz.o\
        mod_nonlocalfile_ncpp.o\
        mod_nonlocalfile_paw.o\
        mod_elementname.o\
        mod_nonlocal_data.o\
        mod_atomxsf.o\
        mod_nonlocalpotential.o\
        mod_bmatrix_lib.o\
        mod_singvaldecomp.o\
        mod_bmatrix.o\
        mod_selfenergy_lib.o\
        mod_selfenergy.o\
        mod_couplingmatrix.o\
        mod_gbloch_lib.o\
        mod_gbloch.o\
        mod_expectationvalue.o\
        mod_incidentwave.o\
        mod_greenfunction_lib.o\
        mod_rsdecomp2blacs.o\
        mod_blacs2rsdecomp.o\
        mod_nonlocal.o\
        mod_rsfd_coefficient.o\
        mod_cgsolver_kake.o\
        mod_cgsolver_precon.o\
        mod_cgsolver.o\
        mod_greenfunction.o\
        mod_scatterwave.o\
        mod_inversematrix.o\
        mod_coefficient.o\
        mod_sqrtmatrix.o\
        mod_channel.o\
        prog_juobm.o


#-- Executive name
EXEC := juOBM


#-- File search path
VPATH = ./:$(ROOTDIR)/juOBM/:$(ROOTDIR)/common/:$(ROOTDIR)/juOBM/src/:$(ROOTDIR)/common/src/


#--
.PHONY: all clean clean_all clean-all


#--
all: $(EXEC)


#-- Link command line to generate executive
$(EXEC): $(OBJS)
	$(FC) $(FFLAGS) -o $@ $^ $(LDFLAGS)


#--
%.o: %.F90
	$(FC) $(FFLAGS) $(PREPROC) -c $<


%.o: %.f
	$(FC) $(FFLAGS) $(PREPROC) -c $<


clean_all clean-all: clean
	rm -f $(EXEC) gmon.out *.dat *.info *.data *.txt *.xsf


clean:
	rm -f *.o *.mod *.optrpt *~


#--
#-- end of Makefile
#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
